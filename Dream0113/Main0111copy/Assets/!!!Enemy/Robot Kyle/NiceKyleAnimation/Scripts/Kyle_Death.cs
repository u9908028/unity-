﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kyle_Death : MonoBehaviour
{
    public bool death_1;
    public bool death_2;
    public Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(death_1 == true)
        {
            anim.SetTrigger("death_1");
        }

        if (death_2 == true)
        {
            anim.SetTrigger("death_2");
        }

    }
    
}
