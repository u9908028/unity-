﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LBJumpShockWave : MonoBehaviour
{
    [Header("Object Pool")]
    public LBJumpShockWavePool LBJumpShockWavePool;
    [Header("Stretch Speed")]
    public float f_stretchSpeed=20.0f;
    [Header("Effect Life")]
    public float effectLifeTime = 1.2f;
    public float _timer;
    [Header("Collider")]
    public Collider thisCollider;
    [Header("Sound Effect")]
    public AudioSource as_soundEffect;

    // Start is called before the first frame update
    void Awake()
    {        
        LBJumpShockWavePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LBJumpShockWavePool>();
        as_soundEffect = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ColliderStretch();
        ReturnToPool();
    }

    public void ColliderStretch()
    {
        transform.localScale += new Vector3(f_stretchSpeed, f_stretchSpeed, f_stretchSpeed) * Time.deltaTime;
        if (transform.localScale.x >= 38)
        {
            f_stretchSpeed = 0;
        }
    }
    public void OnEnable()
    {
        _timer = Time.time;
        transform.localScale = new Vector3(4f, 4f, 4f);
        f_stretchSpeed = 40;
        thisCollider.enabled = true;
        as_soundEffect.Play();
    }
    public void OnDisable()
    {
        as_soundEffect.Stop();
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }

        if (Time.time > _timer + effectLifeTime)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            LBJumpShockWavePool.Recovery(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Controller")
        {
            thisCollider.enabled = false;
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
    }

}
