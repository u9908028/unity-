﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
using UnityEngine.Playables;
namespace LaserMaker
{
    public class LaserMaker : MonoBehaviour
    {

        public AI_data data;
        public LaserPool laserPool;
        public FightSystem fightSystem;
        public TimelineManager boss_timelineManager;
        

        //射出來射設定
        [Header("Laser Making Time Settings")]
        public float f_howManyTimeToMakeLaser = 5f;//設定多少時間生成一個雷射
        public float f_laserMakeTimer = 0;//計時器

        [Header("Laser Making Position Settings")]
        public LaserWillShootPlaceCirclePool laserWillShootPlaceCirclePool;//預設射出位置的圓圈
        public GameObject go_laserMakerCenter;//這個雷射產生器的中心
        public Vector3 v_laserWillShootPosition;//產生雷射的位置，那個圈圈
        public Vector3 v_laserMakedPosition;//高空中的雷射的位置
        public float f_rangeOfLaserRandomMake = 10f;//以中心為多少範圍
        public int i_howManyLaserWillShootInOneWave=2;//陣列的數量，一波同時射幾個
        public Vector3[] v_howManyLaserRandomMakePosition;//存圈圈位置的陣列
        public Vector3[] v_howManyLaserRandomMakePositionInSky;//存天空雷射的
        public float f_timerInSameWave=0;//一波內的時間差的計時器
        public float f_howManyTimeMakeALaserInSameWave=0.2f;//一波內的時間差設定
        [SerializeField]
        private int i_IndexOfVector=0;//陣列的編號
        [Tooltip("瞄準玩家前方的向量大小，Random一個值=設定的+5f")]
        public float f_aimingPlayerForwardPositionRange = 8f;
        

        //跟隨玩家的位置
        [Header("Player Following Settings")]
        public GameObject controller;//玩家
        public float f_laserMakerStartFollowPlayerDistance = 30f;//離玩家多少距離之內開始追蹤玩家

        //關機
        public bool b_laserMakerShutDown;//開始射了沒
        public bool b_ForLastBoss;         
        public bool b_ForHeavenRoad;
        public float f_distanceToLastBossToShuntDown=50f;
        
        

        // Start is called before the first frame update
        void Start()
        {
            controller = GameObject.FindGameObjectWithTag("Controller");
            go_laserMakerCenter = this.gameObject;
            laserWillShootPlaceCirclePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserWillShootPlaceCirclePool>();
            laserPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserPool>();

            v_howManyLaserRandomMakePosition = new Vector3[i_howManyLaserWillShootInOneWave];
            v_howManyLaserRandomMakePositionInSky = new Vector3[i_howManyLaserWillShootInOneWave];

            data = GameObject.Find("Boss").GetComponent<AI_data>();
            fightSystem = GameObject.FindGameObjectWithTag("FightSystem").GetComponent<FightSystem>();
            boss_timelineManager = GameObject.Find("AnimEvent_05_boss登場Tri").GetComponent<TimelineManager>();
            
        }

        // Update is called once per frame
        void Update()
        {
            LaserMakerSwitch();

            if (!b_laserMakerShutDown)
            {
                FollowWithPlayer();
                ShootPatternChange();
            }
        }

        /// <summary>
        /// 跟隨主角位置
        /// </summary>
        public void FollowWithPlayer()
        {
            float _distance = Vector3.Distance(controller.transform.position, transform.position);
            if (_distance < f_laserMakerStartFollowPlayerDistance)
            {
                transform.position = controller.transform.position;
            }
        }

        /// <summary>
        /// 產生一個向量的陣列，以雷射產生機為中心Random X Z座標
        /// 回傳到公開變數 v_howManyLaserRandomMakePosition
        /// 另外同時產生一個生成在空中的位置和地上的圓圈做配對
        /// 回傳到公開變數 v_howManyLaserRandomMakePositionInSky
        /// </summary>
        public void MakeRandomPositions()
        {
            for (int i=0; i<i_howManyLaserWillShootInOneWave; i++)
            {
                v_howManyLaserRandomMakePosition[i]= controller.transform.position + controller.transform.forward * Random.Range(f_aimingPlayerForwardPositionRange, f_aimingPlayerForwardPositionRange+5f) +
                new Vector3(Random.Range(f_rangeOfLaserRandomMake, -f_rangeOfLaserRandomMake), 0.1f, Random.Range(f_rangeOfLaserRandomMake, -f_rangeOfLaserRandomMake));
                v_howManyLaserRandomMakePositionInSky[i] = v_howManyLaserRandomMakePosition[i] + new Vector3(0.0f, 40f, 0.0f);
            }            
        }

        /// <summary>
        /// 備用
        /// </summary>
        public void MakeRandomPositions2()
        {
            for (int i = 0; i < i_howManyLaserWillShootInOneWave; i++)
            {
                v_howManyLaserRandomMakePosition[i]= controller.transform.position + controller.transform.forward *3.0f +
                    new Vector3(Random.Range(f_rangeOfLaserRandomMake, -f_rangeOfLaserRandomMake), 0.1f, Random.Range(f_rangeOfLaserRandomMake, -f_rangeOfLaserRandomMake));
            }
        }

        /// <summary>
        /// 瞄準主角位置，直的往下射，一根
        /// //一波發射速度設定
        /// </summary>
        public void ShootLaserPattern1()
        {
            

            f_laserMakeTimer += Time.deltaTime;
            v_laserWillShootPosition = controller.transform.position + controller.transform.forward * Random.Range(1.0f, 2.0f) + new Vector3(0f, 0.1f, 0f);
            
            if (f_laserMakeTimer > f_howManyTimeToMakeLaser)
            {
                MakeLaserWillShootCircle();
                //高空中的雷射位置
                v_laserMakedPosition = v_laserWillShootPosition + new Vector3(0f, 40f, 0f);
                StartCoroutine(ShootLaser());
                f_laserMakeTimer = 0;                               
            }
        }

        /// <summary>
        /// 隨機瞄準，直的往下射，一根
        /// //一波發射速度設定
        /// //發射隨機範圍設定
        /// </summary>
        public void ShootLaserPattern2()
        {
            

            f_laserMakeTimer += Time.deltaTime;
            v_laserWillShootPosition = controller.transform.position + controller.transform.forward * Random.Range(0.0f, 2.0f) +
                new Vector3(Random.Range(f_rangeOfLaserRandomMake, -f_rangeOfLaserRandomMake), 0.1f, Random.Range(f_rangeOfLaserRandomMake, -f_rangeOfLaserRandomMake));
            
            if (f_laserMakeTimer > f_howManyTimeToMakeLaser)
            {
                MakeLaserWillShootCircle();
                //高空中的雷射位置
                v_laserMakedPosition = v_laserWillShootPosition + new Vector3(0f, 40f, 0f);
                StartCoroutine(ShootLaser());
                f_laserMakeTimer = 0;
            }
        }

        /// <summary>
        /// 瞄準主角位置+隨機位置，同時多根直的
        /// //一波發射速度設定
        /// //發射數量設定(一開始就要設定好，不然動態陣列會很吃效能)
        /// //發射隨機範圍(大才不會擠在一起)
        /// </summary>
        public void ShootLaserPattern3()
        {
            


            f_laserMakeTimer += Time.deltaTime;
     
            if (f_laserMakeTimer > f_howManyTimeToMakeLaser)
            {
                MakeRandomPositions();
                //產生多個圓圈在地上
                MakeLaserWillShootCircle(v_howManyLaserRandomMakePosition);//多個地點
                v_laserWillShootPosition = controller.transform.position + new Vector3(0f, 0.1f, 0f);
                MakeLaserWillShootCircle();//主角位置

                //產生多個雷射在天上
                StartCoroutine(ShootLaser(v_howManyLaserRandomMakePositionInSky));
                v_laserMakedPosition = v_laserWillShootPosition + new Vector3(0f, 40f, 0f);
                StartCoroutine(ShootLaser());//主角位置

                f_laserMakeTimer = 0;
            }
        }

        /// <summary>
        /// 同一波內時間差一直瞄準主角位置
        /// //一波發射時間設定
        /// //一波發射數量設定(一開始就要設定好，不然動態陣列會很吃效能)
        /// //發射隨機範圍設定(0.1秒瞄準一個，所以不要範圍太大才能瞄準主角)
        /// </summary>
        public void ShootLaserPattern4()
        {

            f_laserMakeTimer += Time.deltaTime;

            if (f_laserMakeTimer > f_howManyTimeToMakeLaser)
            {
                MakeRandomPositions();
                f_timerInSameWave += Time.deltaTime;

                if (f_timerInSameWave > f_howManyTimeMakeALaserInSameWave)
                {
                    v_laserWillShootPosition = v_howManyLaserRandomMakePosition[i_IndexOfVector];
                    v_laserMakedPosition = v_howManyLaserRandomMakePositionInSky[i_IndexOfVector];
                    
                    MakeLaserWillShootCircle(v_laserWillShootPosition);
                    StartCoroutine(ShootLaser(v_laserMakedPosition));

                    i_IndexOfVector++;
                    if (i_IndexOfVector >= v_howManyLaserRandomMakePosition.Length)
                    {
                        i_IndexOfVector = 0;
                        f_laserMakeTimer = 0;
                    }
                    f_timerInSameWave = 0;              
                }               
            }
        }

        /// <summary>
        /// 備用
        /// </summary>
        public void ShootLaserPattern5()
        {
            f_laserMakeTimer += Time.deltaTime;

            if (f_laserMakeTimer > f_howManyTimeToMakeLaser)
            {
                MakeRandomPositions();
                f_timerInSameWave += Time.deltaTime;

                if (f_timerInSameWave > f_howManyTimeMakeALaserInSameWave)
                {
                    v_laserWillShootPosition = v_howManyLaserRandomMakePosition[i_IndexOfVector];
                    v_laserMakedPosition = v_howManyLaserRandomMakePositionInSky[i_IndexOfVector];

                    MakeLaserWillShootCircle(v_laserWillShootPosition);
                    StartCoroutine(ShootLaser(v_laserMakedPosition));

                    i_IndexOfVector++;
                    if (i_IndexOfVector >= v_howManyLaserRandomMakePosition.Length)
                    {
                        i_IndexOfVector = 0;
                        f_laserMakeTimer = 0;
                    }
                    f_timerInSameWave = 0;
                }
            }
        }


        /// <summary>
        /// 射出一個圓圈，於主角腳下
        /// </summary>
        public void MakeLaserWillShootCircle()
        {
            laserWillShootPlaceCirclePool.ReUse(  v_laserWillShootPosition, Quaternion.identity);
        }
        /// <summary>
        /// 射出一個圓圈，於主角腳下，可丟向量進去
        /// </summary>
        public void MakeLaserWillShootCircle(Vector3 willShootPosition)
        {
            laserWillShootPlaceCirclePool.ReUse(willShootPosition, Quaternion.identity);
        }
        /// <summary>
        /// 多根射出的圓圈，會同時
        /// </summary>
        /// <param name="willShootPosition"></param>
        public void MakeLaserWillShootCircle(Vector3[] willShootPosition)
        {
            for (int i=0; i<willShootPosition.Length; i++)
            {
                laserWillShootPlaceCirclePool.ReUse(willShootPosition[i], Quaternion.identity);
            }
            
        }
        /// <summary>
        /// 發射一個，主角位置
        /// </summary>
        public void MakeLaser()
        {
            laserPool.ReUse( v_laserMakedPosition, Quaternion.Euler(180f, 0f, 0f));
        }
        /// <summary>
        /// 射一個，可以丟向量
        /// </summary>
        /// <param name="willShootPosition"></param>
        public void MakeLaser(Vector3 willShootPosition)
        {
            laserPool.ReUse(willShootPosition, Quaternion.Euler(180f, 0f, 0f));
        }
        /// <summary>
        /// 發射多個
        /// </summary>
        /// <param name="willShootPosition"></param>
        public void MakeLaser(Vector3[] willShootPosition)
        {
            for (int i=0; i<willShootPosition.Length; i++)
            {
                laserPool.ReUse(willShootPosition[i], Quaternion.Euler(180f, 0f, 0f));
            }
        }

        public IEnumerator ShootLaser()
        {
            yield return new WaitForSeconds(0.1f);
            MakeLaser();
        }
        /// <summary>
        /// 發射多個，同時
        /// </summary>
        /// <param name="willShootPosition"></param>
        /// <returns></returns>
        public IEnumerator ShootLaser(Vector3[] willShootPosition)
        {
            yield return new WaitForSeconds(0.1f);
            MakeLaser(willShootPosition);
        }
        /// <summary>
        /// 只發射一個，可丟向量
        /// </summary>
        /// <returns></returns>
        public IEnumerator ShootLaser(Vector3 willShootPosition)
        {
            yield return new WaitForSeconds(0.1f);
            MakeLaser(willShootPosition);
        }

        /// <summary>
        /// 射擊樣式的變換
        /// </summary>
        public void ShootPatternChange()
        {
            if (b_ForLastBoss)
            {
                if (b_ForHeavenRoad) { b_laserMakerShutDown = true; }
                if( data.GetHealthRate() >= 0.5f )
                {
                    f_howManyTimeToMakeLaser = 5f;
                    ShootLaserPattern1();                   
                }
                else if (data.GetHealthRate() >= 0.25f && data.GetHealthRate() < 0.5f )
                {
                    f_howManyTimeToMakeLaser = 8f;
                    f_rangeOfLaserRandomMake = 10f;
                    ShootLaserPattern3();
                }
                else if(data.GetHealthRate() <=0.25f && data.GetHealthRate() >0f)
                {
                    f_howManyTimeToMakeLaser = 6f;
                    f_rangeOfLaserRandomMake = 5f;
                    ShootLaserPattern4();
                }
                else if (data.GetHealthRate() <= 0f  )
                {
                    b_laserMakerShutDown = true;
                }
            }    
            else if (b_ForHeavenRoad)
            {
                if (b_ForLastBoss) { b_laserMakerShutDown = true; }
                f_howManyTimeToMakeLaser = 3.0f;
                f_rangeOfLaserRandomMake = 1f;
                ShootLaserPattern4();
            }
        }

        /// <summary>
        /// 雷射產生器的開關
        /// </summary>
        public void LaserMakerSwitch()
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                b_laserMakerShutDown = false;
            }
            else if (Input.GetKeyDown(KeyCode.I))
            {
                b_laserMakerShutDown = true;
            }

            if (fightSystem.currentHp <= 0)
            {
                b_laserMakerShutDown = true;
                return;
            }

            if (b_ForHeavenRoad)
            {
                b_ForLastBoss = false;
                if (fightSystem.currentHp <= 0)
                {
                    b_laserMakerShutDown = true;
                    return;
                }
                else if( boss_timelineManager.b_hasBeenTiggered || boss_timelineManager.timelineAnimation.state == PlayState.Playing)
                {
                    b_laserMakerShutDown = true;
                }
                else if (Vector3.Distance(this.transform.position, data.enemy.transform.position) < f_distanceToLastBossToShuntDown)
                {
                    b_laserMakerShutDown = true;
                }
                else if (Vector3.Distance(this.transform.position, controller.transform.position) < f_laserMakerStartFollowPlayerDistance)
                {
                    b_laserMakerShutDown = false;
                }
            }
            else if (b_ForLastBoss)
            {
                b_ForHeavenRoad = false;
                if (fightSystem.currentHp <= 0)
                {
                    b_laserMakerShutDown = true;
                    return;
                }
                else if (Vector3.Distance(this.transform.position, data.enemy.transform.position) > 50f)
                {
                    b_laserMakerShutDown = true;
                }
                else if(Vector3.Distance(this.transform.position, controller.transform.position) < f_laserMakerStartFollowPlayerDistance)
                {
                    b_laserMakerShutDown = false;
                }
                
            }

        }


        public void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(transform.position, transform.position+transform.forward);
            if (!b_laserMakerShutDown)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(transform.position, f_laserMakerStartFollowPlayerDistance);
            }
            else
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(transform.position, f_laserMakerStartFollowPlayerDistance);
            }
            
        }
    }
}