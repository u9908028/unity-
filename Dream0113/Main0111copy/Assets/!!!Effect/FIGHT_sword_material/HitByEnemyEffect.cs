﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitByEnemyEffect : MonoBehaviour
{
    public HitByEnemyEffectPool hitByEnemyEffectPool;
    [SerializeField]
    float _timer;
    public float f_effectLife=1.3f;
    // Start is called before the first frame update
    void Start()
    {
        hitByEnemyEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<HitByEnemyEffectPool>();
    }

    // Update is called once per frame
    void Update()
    {
        EffectReturnToPool();
    }


    private void OnEnable()
    {
        _timer = Time.time;
    }

    private void EffectReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time > _timer+f_effectLife)
        {
            hitByEnemyEffectPool.Recovery(this.gameObject);
        }
    }
}
