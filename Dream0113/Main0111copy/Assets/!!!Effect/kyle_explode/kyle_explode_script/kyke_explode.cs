﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kyke_explode : MonoBehaviour
{
   public GameObject nocrash;
   //public GameObject fractObj;
    //public GameObject crashObject;//要爆出的東西
    public Explo_kyle_Pool Pool;//要丟爆炸凱爾物件池用
    public ParticlePool particePool;//爆炸凱爾特效物件池使用
    public float minForce;
    public float maxForce;
    public float radius;
    // public GameObject crashEffect;//要爆出的特效
    // Start is called before the first frame update
    void Start()
    {   
        
        nocrash=this.gameObject;//抓到他自己的gameObject
        Pool=GameObject.Find("ObjectPool").GetComponent<Explo_kyle_Pool>();//爆炸凱爾物件池呼叫
        //ExplosionFunc();
        particePool=GameObject.Find("ObjectPool").GetComponent<ParticlePool>();//爆炸凱爾特效物件池呼叫
    }


    void ExplosionFunc(){
        Destroy(nocrash);
        //Instantiate(crashEffect,this.transform.position,Quaternion.identity);
        //Destroy(crashEffect);

        GameObject particegameObject=  particePool.ReUse(this.transform.position,Quaternion.identity) ;
        StartCoroutine(effectPlay(particegameObject));
       //nocrash.SetActive(false);
       /// GameObject fractObj=Instantiate(crashObject,this.transform.position,Quaternion.identity) as GameObject;
       GameObject gameObject=  Pool.ReUse(this.transform.position,Quaternion.identity) ;
       
        // foreach(Transform t in fractObj.transform){
        //     var rb=t.GetComponent<Rigidbody>();
        //     if(rb!=null){
        //         print("有爆炸");
        //         rb.AddExplosionForce(Random.Range( minForce,maxForce),transform.position,radius);
               
        //     }
        // }
        foreach(Transform t in gameObject.transform){
            var rb=t.GetComponent<Rigidbody>();
            if(rb!=null){
                //print("有爆炸");
                rb.AddExplosionForce(Random.Range( minForce,maxForce),transform.position,radius);
               
            }
        }
        
         Destroy(gameObject,7f);
         
        
       
    }
     IEnumerator effectPlay(GameObject effect){
       yield return new WaitForSeconds(0.2f);
       Pool.Recovery(effect);
       //Destroy(effect);
   }
}
