﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SA;
using Bubble;

[CustomEditor(typeof(AI_data))]
[CanEditMultipleObjects]
public class AI_data_Editor : Editor
{
    #region 舊editor
    //private bool foldForFindObject = true;
    //private bool foldChaseMove = true;
    //private bool foldTimer = true;
    //private bool foldForBackRelation = true;
    //private bool foldHp = true;
    //private bool foldPlayerDamage = true;
    //private bool foldRange = true;
    //private bool foldAttack = true;
    //private bool foldShoot = true;
    //private bool foldShield;
    //private bool foldTakeDamage;
    //private bool foldEnemyState = true;
    //private bool foldLastBoss = true;
    //private bool foldNiceKyle = true;
    //private bool foldDefaultInspector;
    //private bool foldAllCustomEditor;

    //public override void OnInspectorGUI()
    //{
    //    serializedObject.Update();
    //    //下面這個會顯示出原本沒有編輯過的樣子
    //    //base.OnInspectorGUI();
    //    //初始化去抓AI_data
    //    AI_data data = target as AI_data;

    //    GUI.backgroundColor = new Color(189f / 255f, 252f / 255f, 201f / 255f);
    //    EditorGUILayout.BeginVertical(GUI.skin.box);
    //    foldDefaultInspector = EditorGUILayout.ToggleLeft("要不要劃出AI_data原來的樣子", foldDefaultInspector);
    //    //foldAllCustomEditor = EditorGUILayout.Toggle("摺疊新的全部頁面", foldAllCustomEditor);
    //    if (GUILayout.Button("摺疊新的全部頁面"))
    //    {
    //        foldForFindObject = false;
    //        foldChaseMove = false;
    //        foldTimer = false;
    //        foldForBackRelation = false;
    //        foldHp = false;
    //        foldPlayerDamage = false;
    //        foldRange = false;
    //        foldAttack = false;
    //        foldShoot = false;
    //        foldShield = false;
    //        foldTakeDamage = false;
    //        foldEnemyState = false;
    //        foldLastBoss = false;
    //        foldNiceKyle = false;
    //    }
    //    EditorGUILayout.EndVertical();

    //    if (foldDefaultInspector == true)
    //    {
    //        GUI.backgroundColor = new Color(240f / 255f, 255f / 255f, 255f / 255f);
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆");
    //        //DrawDefaultInspector();
    //        base.OnInspectorGUI();
    //        EditorGUILayout.EndVertical();
    //    }
    //    GUI.backgroundColor = Color.white;



    //    //尋找各種東西
    //    foldForFindObject = EditorGUILayout.Foldout(foldForFindObject, "★★★★★ 抓取各個需要的東西  ★★★★★");
    //    if (foldForFindObject)
    //    {
    //        EditorGUILayout.LabelField("這隻怪獸自己本身");
    //        data.enemy = (GameObject)EditorGUILayout.ObjectField("enemy", data.enemy, typeof(GameObject), true);
    //        EditorGUILayout.LabelField("這隻怪獸自己的Collider");
    //        data.thisEnemyCollider = (Collider)EditorGUILayout.ObjectField("thisEnemyCollider", data.thisEnemyCollider, typeof(Collider), true);
    //        EditorGUILayout.LabelField("射泡泡的腳本");
    //        data.bubbleShooter = (BubbleShoot_3)EditorGUILayout.ObjectField("thisEnemyCollider", data.bubbleShooter, typeof(BubbleShoot_3), true);
    //        EditorGUILayout.LabelField("玩家");
    //        data.controller = (GameObject)EditorGUILayout.ObjectField("thisEnemyCollider", data.controller, typeof(GameObject), true);
    //        EditorGUILayout.LabelField("EnemyTarget的腳本");
    //        data.enemyTarget = (EnemyTarget)EditorGUILayout.ObjectField("thisEnemyCollider", data.enemyTarget, typeof(EnemyTarget), true);
    //        EditorGUILayout.LabelField("FightSystem的腳本");
    //        data.fightSystem = (FightSystem)EditorGUILayout.ObjectField("thisEnemyCollider", data.fightSystem, typeof(FightSystem), true);
    //        EditorGUILayout.LabelField("補血道具的物件池");
    //        data.healthPool = (HealthPool)EditorGUILayout.ObjectField("thisEnemyCollider", data.healthPool, typeof(HealthPool), true);
    //        EditorGUILayout.LabelField("怪獸身上的對話視窗上的腳本");
    //        data.kaiwaWindow = (KaiwaWindow)EditorGUILayout.ObjectField("thisEnemyCollider", data.kaiwaWindow, typeof(KaiwaWindow), true);
    //        EditorGUILayout.LabelField("這隻怪獸身上的Animator");
    //        data.anim = (Animator)EditorGUILayout.ObjectField("anim", data.anim, typeof(Animator), true);

    //    }

    //    EditorGUILayout.Space();

    //    foldChaseMove = EditorGUILayout.Foldout(foldChaseMove, "★★★★★ 追蹤、移動  ★★★★★");
    //    if (foldChaseMove)
    //    {
    //        EditorGUILayout.LabelField("判斷障礙物的射線長度");
    //        data.rayDistance = EditorGUILayout.FloatField("rayDistance", data.rayDistance);
    //        EditorGUILayout.LabelField("追擊的走路速度，計算用");
    //        data.Chase_speed = EditorGUILayout.FloatField("Chase_speed", data.Chase_speed);
    //        EditorGUILayout.LabelField("凱爾的追擊速度，設定用");
    //        data.kyleSpeed = EditorGUILayout.FloatField("kyleSpeed", data.kyleSpeed);
    //        EditorGUILayout.LabelField("視角範圍");
    //        data.SightRange = EditorGUILayout.FloatField("SightRange", data.SightRange);
    //        EditorGUILayout.LabelField("面對玩家的轉動速度，計算用");
    //        data.Rotationspeed = EditorGUILayout.FloatField("Rotationspeed", data.Rotationspeed);
    //        EditorGUILayout.LabelField("凱爾追擊時的轉動速度，設定用");
    //        data.kyleRotation = EditorGUILayout.FloatField("Rotationspeed", data.kyleRotation);
    //        EditorGUILayout.LabelField("往前的速度、向量，監看用");
    //        data.movementVec = EditorGUILayout.Vector3Field("movementVec", data.movementVec);
    //        EditorGUILayout.LabelField("往前的速度，與玩家之間的向量，監看用");
    //        data.dirToPlayer = EditorGUILayout.Vector3Field("dirToPlayer", data.dirToPlayer);
    //        EditorGUILayout.LabelField("是不是在追擊");
    //        data.isChase = EditorGUILayout.Toggle("isChase", data.isChase);
    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
    //        EditorGUILayout.LabelField("下次追擊的計時器");
    //        data.float_nextChase = EditorGUILayout.FloatField("float_nextChase", data.float_nextChase);
    //        EditorGUILayout.LabelField("下次追擊的時間設定");
    //        data.float_chaseRate = EditorGUILayout.FloatField("float_chaseRate", data.float_chaseRate);
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //    }

    //    EditorGUILayout.Space();

    //    foldTimer = EditorGUILayout.Foldout(foldTimer, "★★★★★ 各種計時器與時間設定  ★★★★★");
    //    if (foldTimer)
    //    {

    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("凱爾倒下的計時器");
    //        //data.fallinBackTime = EditorGUILayout.FloatField("fallinBackTime", data.fallinBackTime);
    //        EditorGUILayout.LabelField("凱爾倒下的時間設定");
    //        data.fallingBackWaitTime = EditorGUILayout.FloatField("fallingBackWaitTime", data.fallingBackWaitTime);
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("射擊狀態要射多久變走路的計時器，泡泡蜘蛛、雷射蜘蛛用");
    //        data.shootingToWalkingTimer = EditorGUILayout.FloatField("shootingToWalkingTimer", data.shootingToWalkingTimer);
    //        EditorGUILayout.LabelField("射擊狀態要射多久變走路的時間設定，泡泡蜘蛛、雷射蜘蛛用");
    //        data.shootingToWalkingTimeSetting = EditorGUILayout.FloatField("shootingToWalkingTimeSetting", data.shootingToWalkingTimeSetting);
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("攻擊狀態要打多久去走路的計時器，凱爾、長腿用");
    //        //data.attackToWalkingTimer = EditorGUILayout.FloatField("attackToWalkingTimer", data.attackToWalkingTimer);
    //        EditorGUILayout.LabelField("攻擊狀態要打多久去走路的時間設定，凱爾、長腿用");
    //        //data.attackToWalkingTimeSetting = EditorGUILayout.FloatField("attackToWalkingTimeSetting", data.attackToWalkingTimeSetting);
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

    //    }

    //    EditorGUILayout.Space();

    //    foldForBackRelation = EditorGUILayout.Foldout(foldForBackRelation, "★★★★★ 後退相關  ★★★★★");
    //    if (foldForBackRelation)
    //    {
    //        EditorGUILayout.LabelField("是不是在後退範圍");
    //        data.inBackRange = EditorGUILayout.Toggle("inBackRange", data.inBackRange);
    //        EditorGUILayout.LabelField("是不是在後退狀態");
    //        data.isBack = EditorGUILayout.Toggle("isBack", data.isBack);

    //        EditorGUILayout.LabelField("往後退的走路速度");
    //        data.backSpeed = EditorGUILayout.FloatField("backSpeed", data.fallingBackWaitTime);
    //        EditorGUILayout.LabelField("進入往後退狀態的範圍");
    //        data.backRang = EditorGUILayout.FloatField("backRang", data.backRang);

    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("往後退狀態的計時器，泡泡蜘蛛用");
    //        data.backWaitTimer = EditorGUILayout.FloatField("backWaitTimer", data.backWaitTimer);
    //        EditorGUILayout.LabelField("往後退狀態的時間設定，要等幾秒，泡泡蜘蛛用");
    //        data.backWaitTimeSetting = EditorGUILayout.FloatField("backWaitTimeSetting", data.backWaitTimeSetting);
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("往後退狀態要退多久的計時器，長腿用");
    //        data.backWaitTimerMelle = EditorGUILayout.FloatField("backWaitTimerMelle", data.backWaitTimerMelle);
    //        EditorGUILayout.LabelField("往後退狀態要退多久的時間設定，長腿用");
    //        data.backWaitTimeMelleSetting = EditorGUILayout.FloatField("backWaitTimeMelleSetting", data.backWaitTimeMelleSetting);
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //    }

    //    EditorGUILayout.Space();

    //    foldHp = EditorGUILayout.Foldout(foldHp, "★★★★★ 血量  ★★★★★");
    //    if (foldHp)
    //    {
    //        EditorGUILayout.LabelField("怪獸的最大血量");
    //        data.maxHp = EditorGUILayout.FloatField("maxHp", data.maxHp);
    //        EditorGUILayout.LabelField("怪獸現在的血量");
    //        data.currentHp = EditorGUILayout.FloatField("currentHp", data.currentHp);
    //        EditorGUILayout.LabelField("怪獸是不是死了");
    //        data.isDead = EditorGUILayout.Toggle("isDead", data.isDead);
    //        EditorGUILayout.LabelField("怪獸死掉過了多久");
    //        data.deathTimer = EditorGUILayout.FloatField("deathTimer", data.deathTimer);
    //    }

    //    EditorGUILayout.Space();

    //    foldPlayerDamage = EditorGUILayout.Foldout(foldPlayerDamage, "★★★★★ 主角的傷害  ★★★★★");
    //    if (foldPlayerDamage)
    //    {
    //        EditorGUILayout.LabelField("主角傷害");
    //        data.C_Damage = EditorGUILayout.FloatField("C_Damage", data.C_Damage);
    //        EditorGUILayout.LabelField("主角打到幾下");
    //        data.onTriCount = EditorGUILayout.IntField("onTriCount", data.onTriCount);
    //    }

    //    EditorGUILayout.Space();

    //    foldRange = EditorGUILayout.Foldout(foldRange, "★★★★★ 距離設定  ★★★★★");
    //    if (foldRange)
    //    {
    //        EditorGUILayout.LabelField("敵人的警戒範圍的距離");
    //        data.float_checkInSight = EditorGUILayout.FloatField("float_checkInSight", data.float_checkInSight);
    //        EditorGUILayout.LabelField("敵人的緩速區的距離");
    //        data.float_SlowDownDistance = EditorGUILayout.FloatField("float_SlowDownDistance", data.float_SlowDownDistance);
    //        EditorGUILayout.LabelField("進入攻擊狀態的範圍 (近戰、遠程皆有用到)");
    //        data.float_AttackRange = EditorGUILayout.FloatField("float_AttackRange", data.float_AttackRange);
    //    }

    //    EditorGUILayout.Space();

    //    foldAttack = EditorGUILayout.Foldout(foldAttack, "★★★★★ 攻擊、近戰設定 ★★★★★");
    //    if (foldAttack)
    //    {
    //        EditorGUILayout.LabelField("是不是進入攻擊狀態");
    //        data.bool_Attack = EditorGUILayout.Toggle("bool_Attack", data.bool_Attack);
    //        EditorGUILayout.Space();
    //        EditorGUILayout.LabelField("有沒有近戰攻擊屬性");
    //        data.bool_EnemyHasMelleAttackProperty = EditorGUILayout.Toggle("bool_EnemyHasMelleAttackProperty", data.bool_EnemyHasMelleAttackProperty);
    //        EditorGUILayout.LabelField("是不是進入到近戰攻擊範圍了");
    //        data.bool_InAttackRange = EditorGUILayout.Toggle("bool_InAttackRange", data.bool_InAttackRange);

    //        EditorGUILayout.LabelField("進入近戰攻擊範圍後，瞄準主角的旋轉速度");
    //        data.float_LookRotationSpeed = EditorGUILayout.FloatField("float_LookRotationSpeed", data.float_LookRotationSpeed);

    //        EditorGUILayout.LabelField("近戰攻擊的傷害");
    //        data.int_MelleAttack_Damage = EditorGUILayout.IntField("int_MelleAttack_Damage", data.int_MelleAttack_Damage);

    //        //下面是畫一個陣列的方法
    //        EditorGUILayout.LabelField("攻擊的動畫陣列");
    //        serializedObject.Update();
    //        EditorGUIUtility.LookLikeInspector();
    //        SerializedProperty data_MelleAttackAnim = serializedObject.FindProperty("MelleAttackAnim");
    //        EditorGUI.BeginChangeCheck();
    //        EditorGUILayout.PropertyField(data_MelleAttackAnim, true);
    //        if (EditorGUI.EndChangeCheck())
    //        {
    //            serializedObject.ApplyModifiedProperties();
    //        }
    //        EditorGUIUtility.LookLikeControls();

    //        EditorGUILayout.Space();

    //        EditorGUILayout.LabelField("進入攻擊狀態後的第一次攻擊前要等多久的計時器");
    //        data.float_FirstAttackCountDown = EditorGUILayout.FloatField("float_FirstAttackCountDown", data.float_FirstAttackCountDown);

    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("近戰攻擊間隔的計時器");
    //        data.float_MelleAttackRate = EditorGUILayout.FloatField("float_MelleAttackRate", data.float_MelleAttackRate);
    //        EditorGUILayout.LabelField("近戰攻擊的下一次攻擊時間設定");
    //        data.float_NextMelleAttack = EditorGUILayout.FloatField("float_NextMelleAttack", data.float_NextMelleAttack);
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

    //        EditorGUILayout.Space();
    //        EditorGUILayout.LabelField("凱爾進入狂暴狀態的血量比例");
    //        data.f_KyleCrazyHealthRate = EditorGUILayout.Slider("f_KyleCrazyHealthRate", data.f_KyleCrazyHealthRate, 0.0f, 0.5f);

    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("凱爾進入狂暴狀態的計時器");
    //        data.f_KyleCrazyTimer = EditorGUILayout.FloatField("f_KyleCrazyTimer", data.f_KyleCrazyTimer);
    //        EditorGUILayout.LabelField("凱爾要在狂暴狀態待多久的時間設定");
    //        data.f_KyleCrazyTimeSetting = EditorGUILayout.FloatField("f_KyleCrazyTimeSetting", data.f_KyleCrazyTimeSetting);
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //    }

    //    EditorGUILayout.Space();

    //    foldShoot = EditorGUILayout.Foldout(foldShoot, "★★★★★ 射擊設定 ★★★★★");
    //    if (foldShoot)
    //    {
    //        EditorGUILayout.LabelField("是否有遠攻屬性");
    //        data.bool_EnemyHasRemoteAttackProperty = EditorGUILayout.Toggle("bool_EnemyHasRemoteAttackProperty", data.bool_EnemyHasRemoteAttackProperty);

    //        EditorGUILayout.LabelField("蜘蛛機器人的炮台");
    //        data.go_spiderTurret = (GameObject)EditorGUILayout.ObjectField("go_spiderTurret", data.go_spiderTurret, typeof(GameObject), true);
    //        EditorGUILayout.LabelField("蜘蛛機器人砲台上的發射口");
    //        data.go_spiderTurretEmitter = (GameObject)EditorGUILayout.ObjectField("go_spiderTurretEmitter", data.go_spiderTurretEmitter, typeof(GameObject), true);
    //        EditorGUILayout.LabelField("蜘蛛機器人砲台的旋轉速度");
    //        data.f_spiderTurretRotationSpeed = EditorGUILayout.FloatField("f_spiderTurretRotationSpeed", data.f_spiderTurretRotationSpeed);
    //        EditorGUILayout.LabelField("蜘蛛機器人瞄準角度的補助用向量");
    //        data.v_spiderAimingAngle = EditorGUILayout.Vector3Field("v_spiderAimingAngle", data.v_spiderAimingAngle);

    //        EditorGUILayout.Space();

    //        EditorGUILayout.LabelField("雷射蜘蛛機器人的雷射物件");
    //        data.go_LaserParent = (GameObject)EditorGUILayout.ObjectField("go_LaserParent", data.go_LaserParent, typeof(GameObject), true);
    //        EditorGUILayout.LabelField("雷射蜘蛛機器人的雷射啟動光束");
    //        data.go_ChargeLaser = (GameObject)EditorGUILayout.ObjectField("go_ChargeLaser", data.go_ChargeLaser, typeof(GameObject), true);
    //        EditorGUILayout.LabelField("雷射蜘蛛機器人的雷射最大威力光束");
    //        data.go_BeamLaser = (GameObject)EditorGUILayout.ObjectField("go_BeamLaser", data.go_BeamLaser, typeof(GameObject), true);
    //        EditorGUILayout.LabelField("雷射蜘蛛機器人的雷射發射口的煙和粒子物件");
    //        data.go_SmokeAndSparks = (GameObject)EditorGUILayout.ObjectField("go_SmokeAndSparks", data.go_SmokeAndSparks, typeof(GameObject), true);
    //        EditorGUILayout.LabelField("雷射蜘蛛機器人的雷射結束光束");
    //        data.go_EndLaser = (GameObject)EditorGUILayout.ObjectField("go_EndLaser", data.go_EndLaser, typeof(GameObject), true);

    //        EditorGUILayout.Space();
    //        EditorGUILayout.LabelField("雷射發射的計時器，Function裡面用\n控制三種雷射啟動→最大→結束用");
    //        data.f_LaserTimer = EditorGUILayout.FloatField("f_LaserTimer", data.f_LaserTimer);

    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("其他狀態要隔多久才可以回到雷射狀態的計時器");
    //        data.f_LaserRechargeTimer = EditorGUILayout.FloatField("f_LaserRechargeTimer", data.f_LaserRechargeTimer);
    //        EditorGUILayout.LabelField("其他狀態要隔多久才可以回到雷射狀態");
    //        data.f_LaserRechargeTimeSetting = EditorGUILayout.FloatField("f_LaserRechargeTimeSetting", data.f_LaserRechargeTimeSetting);
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("發射雷射時，射出Collider的計時器");
    //        data.f_LaserColliderShootTimer = EditorGUILayout.FloatField("f_LaserColliderShootTimer", data.f_LaserColliderShootTimer);
    //        EditorGUILayout.LabelField("發射雷射時，每隔多久進行一次傷害判斷，射出Collider的速度");
    //        data.f_LaserColliderShootSpeed = EditorGUILayout.FloatField("f_LaserColliderShootSpeed", data.f_LaserColliderShootSpeed);
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

    //        EditorGUILayout.Space();
    //        EditorGUILayout.LabelField("泡泡射擊速度，數字越大越慢(時間是倒數的)");
    //        data.float_BubbleShootSpeed = EditorGUILayout.FloatField("float_BubbleShootSpeed", data.float_BubbleShootSpeed);
    //        EditorGUILayout.LabelField("第一次射泡泡的延遲時間");
    //        data.float_FirstShootDelayTime = EditorGUILayout.FloatField("float_FirstShootDelayTime", data.float_FirstShootDelayTime);
    //        EditorGUILayout.LabelField("一波泡泡射擊總時間");
    //        data.float_WaveShootTotalTime = EditorGUILayout.FloatField("float_WaveShootTotalTime", data.float_WaveShootTotalTime);
    //        EditorGUILayout.LabelField("泡泡射擊完之後的Delay時間，Delay完之後若條件達成繼續下一波");
    //        data.float_WaveShootOverDelayTime = EditorGUILayout.FloatField("float_WaveShootOverDelayTime", data.float_WaveShootOverDelayTime);

    //        //下面是顯示陣列的方法
    //        EditorGUILayout.LabelField("泡泡的發射口群組");
    //        //for (int i=0; i<data.transArray_Emiiter.Length; ++i)
    //        //{
    //        //    data.transArray_Emiiter[i] = (Transform)EditorGUILayout.ObjectField("transArray_Emiiter", data.transArray_Emiiter[i], typeof(Transform), true);
    //        //}
    //        //Transform trans = EditorGUILayout.ObjectField("把發射口拉到這", null, typeof(Transform), true) as Transform;
    //        serializedObject.Update();
    //        EditorGUIUtility.LookLikeInspector();
    //        SerializedProperty data_transArray_Emiiter = serializedObject.FindProperty("transArray_Emiiter");
    //        EditorGUI.BeginChangeCheck();
    //        EditorGUILayout.PropertyField(data_transArray_Emiiter, true);
    //        if (EditorGUI.EndChangeCheck())
    //        {
    //            serializedObject.ApplyModifiedProperties();
    //        }
    //        EditorGUIUtility.LookLikeControls();
    //        //////以上這麼多
    //    }

    //    EditorGUILayout.Space();

    //    foldShield = EditorGUILayout.Foldout(foldShield, "★★★★★ 護盾、防護罩設定 ★★★★★");
    //    if (foldShield)
    //    {
    //        EditorGUILayout.LabelField("護盾的腳本");
    //        data.defenseShield = (Shield)EditorGUILayout.ObjectField("defenseShield", data.defenseShield, typeof(Shield), true);
    //        EditorGUILayout.LabelField("有沒有護盾屬性");
    //        data.HaveShield = EditorGUILayout.Toggle("HaveShield", data.HaveShield);
    //        EditorGUILayout.LabelField("護盾的物件");
    //        data.Shield = (GameObject)EditorGUILayout.ObjectField("Shield", data.Shield, typeof(GameObject), true);
    //        EditorGUILayout.LabelField("護盾已打開");
    //        data.shieldOpen = EditorGUILayout.Toggle("shieldOpen", data.shieldOpen);
    //        EditorGUILayout.LabelField("護盾的半徑");
    //        data.shieldRadius = EditorGUILayout.FloatField("shieldRadius", data.shieldRadius);
    //        EditorGUILayout.LabelField("護盾的能量");
    //        data.shieldPower = EditorGUILayout.FloatField("shieldPower", data.shieldPower);

    //    }

    //    EditorGUILayout.Space();

    //    foldTakeDamage = EditorGUILayout.Foldout(foldTakeDamage, "★★★★★ 怪獸受到傷害專區 ★★★★★");
    //    if (foldTakeDamage)
    //    {

    //        EditorGUILayout.LabelField("被武器打到，受到傷害");
    //        data.takeDamage = EditorGUILayout.Toggle("takeDamage", data.takeDamage);
    //        EditorGUILayout.LabelField("被子彈打到");
    //        data.machineDamage = EditorGUILayout.Toggle("machineDamage", data.machineDamage);
    //        EditorGUILayout.LabelField("被劍氣斬打到");
    //        data.bladeFuryDamage = EditorGUILayout.Toggle("bladeFuryDamage", data.bladeFuryDamage);
    //        EditorGUILayout.LabelField("被劍氣或者強攻擊打到倒下了，凱爾用");
    //        data.isFallingBackDown = EditorGUILayout.Toggle("isFallingBackDown", data.isFallingBackDown);
    //        EditorGUILayout.LabelField("倒下之後站起來，凱爾用");
    //        data.isStandUp = EditorGUILayout.Toggle("isStandUp", data.isStandUp);
    //    }

    //    EditorGUILayout.Space();

    //    foldEnemyState = EditorGUILayout.Foldout(foldEnemyState, "★★★★★ 怪獸的狀態遷移 ★★★★★");
    //    if (foldEnemyState)
    //    {
    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.window);
    //        EditorGUILayout.LabelField("❤❤❤❤ 凱爾 ❤❤❤❤");
    //        EditorGUILayout.LabelField("準備要去的狀態");
    //        data.i_KyleStateNumber = EditorGUILayout.IntField("i_KyleStateNumber", data.i_KyleStateNumber);
    //        GUI.backgroundColor = new Color(223.0f / 255.0f, 240f / 255f, 150f / 255f);//萊姆色的                
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("Idle = 0, Chase = 1, Attack = 2, Crazy = 3, FallDown = 9  ");
    //        EditorGUILayout.LabelField("死亡和受傷狀態因為要優先判斷所以沒有編號");
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //        GUI.backgroundColor = Color.white;
    //        EditorGUILayout.Space();


    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.window);
    //        EditorGUILayout.LabelField("@@@@ 長腳叔叔 @@@@");
    //        EditorGUILayout.LabelField("準備要去的狀態");
    //        data.i_LongLegStateNumber = EditorGUILayout.IntField("i_LongLegStateNumber", data.i_LongLegStateNumber);
    //        GUI.backgroundColor = new Color(223.0f / 255.0f, 240f / 255f, 150f / 255f); ;
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("Idle = 0, Chase = 1, Attack = 2, Back = 3  ");
    //        EditorGUILayout.LabelField("死亡狀態因為要優先判斷所以沒有編號");
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //        GUI.backgroundColor = Color.white;
    //        EditorGUILayout.Space();


    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.window);
    //        EditorGUILayout.LabelField("◯◯◯◯ 泡泡蜘蛛機器人 ◯◯◯◯");
    //        EditorGUILayout.LabelField("準備要去的狀態");
    //        data.i_SpiderStateNumber = EditorGUILayout.IntField("i_SpiderStateNumber", data.i_SpiderStateNumber);
    //        GUI.backgroundColor = new Color(223.0f / 255.0f, 240f / 255f, 150f / 255f); ;
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("Idle = 0, Chase = 1, Shoot = 2, Back = 3  ");
    //        EditorGUILayout.LabelField("死亡狀態因為要優先判斷所以沒有編號");
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //        GUI.backgroundColor = Color.white;
    //        EditorGUILayout.Space();


    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.window);
    //        EditorGUILayout.LabelField("◀◀◀◀ 雷射蜘蛛機器人 ▶▶▶▶");
    //        EditorGUILayout.LabelField("準備要去的狀態");
    //        data.i_SpiderStateNumber = EditorGUILayout.IntField("i_SpiderStateNumber", data.i_SpiderStateNumber);
    //        GUI.backgroundColor = new Color(223.0f / 255.0f, 240f / 255f, 150f / 255f);
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("Idle = 0, Chase = 1, ShootLaser = 2, Back = 3  ");
    //        EditorGUILayout.LabelField("死亡狀態因為要優先判斷所以沒有編號");
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //        EditorGUILayout.EndVertical();//畫個不同底色的盒子結束
    //        GUI.backgroundColor = Color.white;
    //        EditorGUILayout.Space();
    //    }

    //    EditorGUILayout.Space();

    //    foldLastBoss = EditorGUILayout.Foldout(foldLastBoss, "★★★★★ 大魔王的設定 ★★★★★");
    //    if (foldLastBoss)
    //    {
    //        EditorGUILayout.LabelField("大魔王的移動速度");
    //        data.f_LBMoveSpeed = EditorGUILayout.FloatField("f_LBMoveSpeed", data.f_LBMoveSpeed);
    //        EditorGUILayout.LabelField("大魔王面對玩家的轉動速度");
    //        data.f_LBRotationSpeed = EditorGUILayout.FloatField("f_LBRotationSpeed", data.f_LBRotationSpeed);

    //        EditorGUILayout.Space();
    //        EditorGUILayout.BeginVertical(GUI.skin.window);
    //        EditorGUILayout.LabelField("大魔王的狀態遷移");
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("大魔王進入狀態之後的計時器");
    //        data.f_LBTimer = EditorGUILayout.FloatField("f_LBTimer", data.f_LBTimer);
    //        EditorGUILayout.LabelField("大魔王接下來要去哪個狀態的編號");
    //        data.i_LBStatePattern = EditorGUILayout.IntField("i_LBStatePattern", data.i_LBStatePattern);
    //        EditorGUILayout.EndVertical();//畫個盒子結束

    //        GUI.backgroundColor = new Color(223.0f / 255.0f, 240f / 255f, 150f / 255f);
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("死亡狀態一定優先判斷所以沒編號");
    //        EditorGUILayout.LabelField("LastBossFirstIdle = 0 ，大魔王將被啟動");
    //        EditorGUILayout.LabelField("LastBossFirstShoot = 4 ，第一次一定會射三顆大球");
    //        EditorGUILayout.LabelField("LastBossIdle1 = 1 ，大魔王的Idle");
    //        EditorGUILayout.LabelField("LastBossJump1 = 7 ，Chase沒追到人過五秒會抽獎看要不要跳一下");
    //        EditorGUILayout.LabelField("LastBossChase1 = 2 ，追擊狀態");
    //        EditorGUILayout.LabelField("LastBossAttackNew1 = 6 ，大魔王的進攻(裡面會抽獎跳)");
    //        EditorGUILayout.LabelField("LastBossShootBigBall = 5 ，射大球狀態");
    //        GUI.backgroundColor = Color.white;
    //        EditorGUILayout.EndVertical();//畫個盒子結束            
    //        EditorGUILayout.EndVertical();//畫個視窗結束

    //        EditorGUILayout.Space();
    //        EditorGUILayout.LabelField("是否可旋轉");
    //        data.b_LBRotating = EditorGUILayout.Toggle("b_LBRotating", data.b_LBRotating);
    //        EditorGUILayout.LabelField("是否可移動");
    //        data.b_LBMoving = EditorGUILayout.Toggle("b_LBMoving", data.b_LBMoving);
    //        EditorGUILayout.LabelField("魔王身上的Rigibody");
    //        data.rb_LBrb = (Rigidbody)EditorGUILayout.ObjectField("rb_LBrb", data.rb_LBrb, typeof(Rigidbody), true);
    //        EditorGUILayout.LabelField("近戰只砍一下的動作之距離設定");
    //        data.f_LBMelleAttackRange1 = EditorGUILayout.FloatField("f_LBMelleAttackRange1", data.f_LBMelleAttackRange1);
    //        EditorGUILayout.LabelField("近戰砍兩下的動作之距離設定");
    //        data.f_LBMelleAttackRange2 = EditorGUILayout.FloatField("f_LBMelleAttackRange2", data.f_LBMelleAttackRange2);
    //        EditorGUILayout.Space();
    //        EditorGUILayout.LabelField("大魔王之射擊距離");
    //        data.f_LBShootAttackRange = EditorGUILayout.FloatField("f_LBShootAttackRange", data.f_LBShootAttackRange);
    //        EditorGUILayout.LabelField("射擊時的Emitter旋轉口是否可旋轉");
    //        data.b_LBEmitterRotate = EditorGUILayout.Toggle("b_LBEmitterRotate", data.b_LBEmitterRotate);

    //        EditorGUILayout.Space();
    //        GUI.backgroundColor = new Color(255.0f / 255.0f, 253f / 255f, 168f / 255f);
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("設定幾次的Idle之後一定要射大球");
    //        data.i_LBHowManyIdleTimesToShootBigBall = EditorGUILayout.IntField("i_LBHowManyIdleTimesToShootBigBall", data.i_LBHowManyIdleTimesToShootBigBall);
    //        GUI.backgroundColor = Color.white;
    //        EditorGUILayout.EndVertical();//畫個盒子結束        


    //        GUI.backgroundColor = new Color(255.0f / 255.0f, 253f / 255f, 168f / 255f);
    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("大魔王進到大魔王專屬的狀態的距離，被啟動的距離");
    //        data.f_LastBossStartActionRange = EditorGUILayout.FloatField("f_LastBossStartActionRange", data.f_LastBossStartActionRange);
    //        GUI.backgroundColor = Color.white;
    //        EditorGUILayout.EndVertical();//畫個盒子結束    
    //        EditorGUILayout.Space();

    //        EditorGUILayout.LabelField("魔王跳起來移動的向量");
    //        data.v_LBJumpTeleport = EditorGUILayout.Vector3Field("v_LBJumpTeleport", data.v_LBJumpTeleport);

    //        EditorGUILayout.Space();

    //        //大魔王泡泡發射口的群組
    //        EditorGUILayout.BeginVertical(GUI.skin.window);

    //        EditorGUILayout.LabelField("各個發射口的Holder，他們的爸爸");
    //        data.go_LBEmitterHolder = (GameObject)EditorGUILayout.ObjectField("go_LBEmitterHolder", data.go_LBEmitterHolder, typeof(GameObject), true);
    //        EditorGUILayout.LabelField("進戰砍一下的發射口Holder");
    //        data.go_LBAttackEmitterHolder = (GameObject)EditorGUILayout.ObjectField("go_LBAttackEmitterHolder", data.go_LBAttackEmitterHolder, typeof(GameObject), true);
    //        EditorGUILayout.Space();

    //        EditorGUILayout.LabelField("跳起來的腳下發射口");
    //        serializedObject.Update();
    //        EditorGUIUtility.LookLikeInspector();
    //        SerializedProperty data_go_LBJumpShoot = serializedObject.FindProperty("go_LBJumpShoot");
    //        EditorGUI.BeginChangeCheck();
    //        EditorGUILayout.PropertyField(data_go_LBJumpShoot, true);
    //        if (EditorGUI.EndChangeCheck())
    //        {
    //            serializedObject.ApplyModifiedProperties();
    //        }
    //        EditorGUIUtility.LookLikeControls();

    //        EditorGUILayout.Space();

    //        EditorGUILayout.LabelField("發射圓圈");
    //        serializedObject.Update();
    //        EditorGUIUtility.LookLikeInspector();
    //        SerializedProperty data_go_LBShootCircle = serializedObject.FindProperty("go_LBShootCircle");
    //        EditorGUI.BeginChangeCheck();
    //        EditorGUILayout.PropertyField(data_go_LBShootCircle, true);
    //        if (EditorGUI.EndChangeCheck())
    //        {
    //            serializedObject.ApplyModifiedProperties();
    //        }
    //        EditorGUIUtility.LookLikeControls();

    //        EditorGUILayout.Space();

    //        EditorGUILayout.LabelField("發射一顆大球");
    //        serializedObject.Update();
    //        EditorGUIUtility.LookLikeInspector();
    //        SerializedProperty data_go_LBShootBigBall = serializedObject.FindProperty("go_LBShootBigBall");
    //        EditorGUI.BeginChangeCheck();
    //        EditorGUILayout.PropertyField(data_go_LBShootBigBall, true);
    //        if (EditorGUI.EndChangeCheck())
    //        {
    //            serializedObject.ApplyModifiedProperties();
    //        }
    //        EditorGUIUtility.LookLikeControls();

    //        EditorGUILayout.Space();

    //        EditorGUILayout.LabelField("發射三顆大球");
    //        serializedObject.Update();
    //        EditorGUIUtility.LookLikeInspector();
    //        SerializedProperty data_go_LBShootBigBall2 = serializedObject.FindProperty("go_LBShootBigBall2");
    //        EditorGUI.BeginChangeCheck();
    //        EditorGUILayout.PropertyField(data_go_LBShootBigBall2, true);
    //        if (EditorGUI.EndChangeCheck())
    //        {
    //            serializedObject.ApplyModifiedProperties();
    //        }
    //        EditorGUIUtility.LookLikeControls();

    //        EditorGUILayout.Space();

    //        EditorGUILayout.LabelField("發射三顆會慢慢消失的持久型大球");
    //        serializedObject.Update();
    //        EditorGUIUtility.LookLikeInspector();
    //        SerializedProperty data_go_LBShootBigBallTernado = serializedObject.FindProperty("go_LBShootBigBallTernado");
    //        EditorGUI.BeginChangeCheck();
    //        EditorGUILayout.PropertyField(data_go_LBShootBigBallTernado, true);
    //        if (EditorGUI.EndChangeCheck())
    //        {
    //            serializedObject.ApplyModifiedProperties();
    //        }
    //        EditorGUIUtility.LookLikeControls();

    //        EditorGUILayout.Space();

    //        EditorGUILayout.EndVertical();//畫個盒子結束    
    //        EditorGUILayout.Space();
    //    }
    //    EditorGUILayout.Space();

    //    foldNiceKyle = EditorGUILayout.Foldout(foldNiceKyle, "★★★★★ 好人凱爾之設定  ★★★★★");
    //    if (foldNiceKyle)
    //    {
    //        EditorGUILayout.LabelField("好人凱爾進入狀態後的計時器");
    //        data.f_NKTimer = EditorGUILayout.FloatField("f_NKTimer", data.f_NKTimer);
    //        EditorGUILayout.LabelField("進入講話的範圍");
    //        data.f_NKSpeakRange = EditorGUILayout.FloatField("f_NKSpeakRange", data.f_NKSpeakRange);
    //        EditorGUILayout.LabelField("走路的速度");
    //        data.f_NKMoveSpeed = EditorGUILayout.FloatField("f_NKMoveSpeed", data.f_NKMoveSpeed);
    //        EditorGUILayout.LabelField("旋轉的速度");
    //        data.f_NKRotateSpeed = EditorGUILayout.FloatField("f_NKRotateSpeed", data.f_NKRotateSpeed);
    //        EditorGUILayout.LabelField("是否允許旋轉");
    //        data.b_NKRotate = EditorGUILayout.Toggle("b_NKRotate", data.b_NKRotate);
    //        EditorGUILayout.Space();
    //        EditorGUILayout.LabelField("狀態的編號");
    //        data.i_NKStateNumber = EditorGUILayout.IntField("i_NKStateNumber", data.i_NKStateNumber);
    //        EditorGUILayout.Space();

    //        EditorGUILayout.LabelField("可不可以移動");
    //        data.b_NKMove = EditorGUILayout.Toggle("b_NKMove", data.b_NKMove);
    //        EditorGUILayout.Space();

    //        EditorGUILayout.LabelField("存敵人物件");
    //        serializedObject.Update();
    //        EditorGUIUtility.LookLikeInspector();
    //        SerializedProperty data_otherEnemies = serializedObject.FindProperty("otherEnemies");
    //        EditorGUI.BeginChangeCheck();
    //        EditorGUILayout.PropertyField(data_otherEnemies, true);
    //        if (EditorGUI.EndChangeCheck())
    //        {
    //            serializedObject.ApplyModifiedProperties();
    //        }
    //        EditorGUIUtility.LookLikeControls();

    //        EditorGUILayout.Space();
    //        EditorGUILayout.LabelField("好人凱爾攻擊範圍");
    //        data.f_NKAttackRange = EditorGUILayout.FloatField("f_NKAttackRange", data.f_NKAttackRange);

    //        EditorGUILayout.Space();
    //        EditorGUILayout.LabelField("存目標移動點");
    //        serializedObject.Update();
    //        EditorGUIUtility.LookLikeInspector();
    //        SerializedProperty data_go_NKTargetPosition = serializedObject.FindProperty("go_NKTargetPosition");
    //        EditorGUI.BeginChangeCheck();
    //        EditorGUILayout.PropertyField(data_go_NKTargetPosition, true);
    //        if (EditorGUI.EndChangeCheck())
    //        {
    //            serializedObject.ApplyModifiedProperties();
    //        }
    //        EditorGUIUtility.LookLikeControls();



    //        EditorGUILayout.LabelField("好人面對目標的向量");
    //        data.v_NKAngleToTarget = EditorGUILayout.Vector3Field("v_NKAngleToTarget", data.v_NKAngleToTarget);
    //        EditorGUILayout.LabelField("好人要移動到目的地的編號");
    //        data.i_NKTargetPositionIndex = EditorGUILayout.IntField("i_NKTargetPositionIndex", data.i_NKTargetPositionIndex);
    //        EditorGUILayout.Space();

    //        EditorGUILayout.BeginVertical(GUI.skin.box);
    //        EditorGUILayout.LabelField("是不是在場上說話的凱爾");
    //        data.b_talkingKyle = EditorGUILayout.Toggle("b_talkingKyle", data.b_talkingKyle);
    //        EditorGUILayout.LabelField("是不是在場上戰鬥的凱爾");
    //        data.b_fightingKyle = EditorGUILayout.Toggle("b_fightingKyle", data.b_fightingKyle);
    //        EditorGUILayout.EndVertical();//畫個盒子結束   
    //    }


    //    EditorGUILayout.Space();
    //    //foldDefaultInspector= EditorGUILayout.Foldout(foldNiceKyle, "AI_data原來的樣子");
    //    //if (foldDefaultInspector)
    //    //{
    //    //    //DrawDefaultInspector();
    //    //    //base.OnInspectorGUI();
    //    //}

    //    serializedObject.ApplyModifiedProperties();
    //}
    #endregion

    #region 新editor
    private AI_data data;
    private EnemyTarget enemyTarget;

    private void OnEnable()
    {
        data = (AI_data)target;
        enemyTarget = data.enemyTarget;
    }

    private bool kyleStatus;
    private bool longLegStatus;
    private bool spiderStatus;
    private bool laserSpiderStatus;
    private bool kyleWallStatus;
    private bool niceKyleStatus;
    private bool defenseTower;
    private bool bossStatus;
    private int i_checkEnemy;

    private bool foldDefaultInspector;//劃出原本的AI_data

    public bool foldRange = true;//範圍類
    public bool foldChase = true;//追擊類
    public bool foldBack = true;//後退類
    public bool foldAttack = true;//攻擊類
    public bool foldShoot = true;//射擊類
    public bool foldTimer = true;//監看用計時器
    public bool foldHP = true;//血量之類的
    public bool foldObject = true;//抓取的腳本
    public bool foldBully = true;//暴打好人

    private void CheckOnlyOneChose()
    {

        switch (i_checkEnemy)
        {
            case 1:
                kyleStatus = true;
                longLegStatus = false;
                spiderStatus = false;
                laserSpiderStatus = false;
                kyleWallStatus = false;
                niceKyleStatus = false;
                defenseTower = false;
                bossStatus = false;
                break;
            case 2:
                longLegStatus = true;
                kyleStatus = false;
                spiderStatus = false;
                laserSpiderStatus = false;
                kyleWallStatus = false;
                niceKyleStatus = false;
                defenseTower = false;
                bossStatus = false;
                break;
            case 3:
                spiderStatus = true;
                kyleStatus = false;
                longLegStatus = false;
                laserSpiderStatus = false;
                kyleWallStatus = false;
                niceKyleStatus = false;
                defenseTower = false;
                bossStatus = false;
                break;
            case 4:
                laserSpiderStatus = true;
                kyleStatus = false;
                longLegStatus = false;
                spiderStatus = false;
                kyleWallStatus = false;
                niceKyleStatus = false;
                defenseTower = false;
                bossStatus = false;
                break;
            case 5:
                kyleWallStatus = true;
                kyleStatus = false;
                longLegStatus = false;
                spiderStatus = false;
                laserSpiderStatus = false;
                niceKyleStatus = false;
                defenseTower = false;
                bossStatus = false;
                break;
            case 6:
                niceKyleStatus = true;
                kyleStatus = false;
                longLegStatus = false;
                spiderStatus = false;
                laserSpiderStatus = false;
                kyleWallStatus = false;
                defenseTower = false;
                bossStatus = false;
                break;
            case 7:
                defenseTower = true;
                kyleStatus = false;
                longLegStatus = false;
                spiderStatus = false;
                laserSpiderStatus = false;
                kyleWallStatus = false;
                niceKyleStatus = false;
                bossStatus = false;
                break;
            case 8:
                bossStatus = true;
                kyleStatus = false;
                longLegStatus = false;
                spiderStatus = false;
                laserSpiderStatus = false;
                kyleWallStatus = false;
                niceKyleStatus = false;
                defenseTower = false;
                break;
        }


    }
    private void ClearChoose()
    {
        kyleStatus = false;
        longLegStatus = false;
        spiderStatus = false;
        laserSpiderStatus = false;
        kyleWallStatus = false;
        niceKyleStatus = false;
        defenseTower = false;
        bossStatus = false;
    }
    private void CheckEnemyTarget()
    {
        if (!data.enemyTarget) { serializedObject.Update(); return; }
        if (data.enemyTarget != null)
        {
            if (data.enemyTarget.b_IsKyle)
            {
                kyleStatus = true;
            }
            else if (data.enemyTarget.b_IsLongLegRobot)
            {
                longLegStatus = true;
            }
            else if (data.enemyTarget.b_IsSpiderRobot)
            {
                spiderStatus = true;
            }
            else if (data.enemyTarget.b_IsLaserSpiderRobot)
            {
                laserSpiderStatus = true;
            }
            else if (data.enemyTarget.b_IsKyleWall)
            {
                kyleWallStatus = true;
            }
            else if (data.enemyTarget.b_IsNiceKyle)
            {
                niceKyleStatus = true;
            }
            else if (data.enemyTarget.b_DefenseTower)
            {
                defenseTower = true;
            }
            else if (data.enemyTarget.b_IsLastBoss)
            {
                bossStatus = true;
            }
            EditorUtility.SetDirty(data);
        }
    }

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        #region Show出原本
        GUI.backgroundColor = new Color(189f / 255f, 252f / 255f, 201f / 255f);
        EditorGUILayout.BeginVertical(GUI.skin.box);
        foldDefaultInspector = EditorGUILayout.ToggleLeft("要不要劃出AI_data原來的樣子", foldDefaultInspector);
        EditorGUILayout.EndVertical();
        if (foldDefaultInspector == true)
        {
            GUI.backgroundColor = new Color(240f / 255f, 255f / 255f, 255f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆");
            //DrawDefaultInspector();
            base.OnInspectorGUI();
            EditorUtility.SetDirty(data);
            EditorGUILayout.EndVertical();
        }
        GUI.backgroundColor = Color.white;
        #endregion

        EditorGUILayout.Space();

        #region 各種怪獸選擇
        GUI.backgroundColor = new Color(255f / 255f, 182f / 255f, 193f / 255f);
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.LabelField("請選擇AI的種類");
        {
            serializedObject.Update();
            kyleStatus = EditorGUILayout.ToggleLeft("壞人凱爾", kyleStatus);
            longLegStatus = EditorGUILayout.ToggleLeft("長腳機器人", longLegStatus);
            spiderStatus = EditorGUILayout.ToggleLeft("泡泡蜘蛛機器人", spiderStatus);
            laserSpiderStatus = EditorGUILayout.ToggleLeft("雷射蜘蛛機器人", laserSpiderStatus);
            kyleWallStatus = EditorGUILayout.ToggleLeft("凱爾隊長", kyleWallStatus);
            niceKyleStatus = EditorGUILayout.ToggleLeft("好人凱爾", niceKyleStatus);
            defenseTower = EditorGUILayout.ToggleLeft("天堂路防禦塔", defenseTower);
            bossStatus = EditorGUILayout.ToggleLeft("BOSS", bossStatus);
            EditorUtility.SetDirty(data);       
            serializedObject.ApplyModifiedProperties();
        }

        //CheckOnlyOneChose();//確認只能選一個
        CheckEnemyTarget();

        if (GUILayout.Button("清空全部的選擇"))
        {
            ClearChoose();
        }
        EditorGUILayout.EndVertical();
        GUI.backgroundColor = Color.white;
        #endregion
        EditorGUILayout.Space();

        #region 全部折疊/打開
        if (GUILayout.Button("全部折疊 / 打開各變數群組"))
        {
            foldRange = !foldRange;//範圍類
            foldChase = !foldChase;//追擊類
            foldBack = !foldBack;//後退類
            foldAttack = !foldAttack;//攻擊類
            foldShoot = !foldShoot;//射擊類
            foldTimer = !foldTimer;//監看用計時器
            foldHP = !foldHP;//血量之類的
            foldObject = !foldObject;//抓取的腳本
            foldBully = !foldBully;//暴打好人
        }
        #endregion

        EditorGUILayout.Space();
        #region 凱爾
        if (kyleStatus)
        {
            #region 狀態
            //狀態
            GUI.backgroundColor = new Color(250f / 255f, 250f / 255f, 210f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.window);
            EditorGUILayout.LabelField("準備要前往的狀態");
            data.i_KyleStateNumber = EditorGUILayout.IntField("i_KyleStateNumber", data.i_KyleStateNumber);
            GUI.backgroundColor = new Color(173f / 255f, 255f / 255f, 47f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("0 → 待機");
            EditorGUILayout.LabelField("1 → 追逐");
            EditorGUILayout.LabelField("2 → 攻擊");
            EditorGUILayout.LabelField("3 → 狂暴");
            EditorGUILayout.LabelField("4 → 暴打好人");
            EditorGUILayout.LabelField("9 → 跌倒");
            EditorGUILayout.LabelField("-1 → Null");
            EditorGUILayout.LabelField("無編號 → 死亡");
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            #endregion

            #region 血量
            //血量
            foldHP = EditorGUILayout.Foldout(foldHP, "★★★★血量設定★★★★");
            if (foldHP)
            {
                EditorGUILayout.LabelField("怪獸的最大血量");
                data.maxHp = EditorGUILayout.FloatField("maxHp", data.maxHp);
                EditorGUILayout.LabelField("怪獸現在的血量");
                data.currentHp = EditorGUILayout.FloatField("currentHp", data.currentHp);
                EditorGUILayout.LabelField("怪獸是不是死了");
                data.isDead = EditorGUILayout.Toggle("isDead", data.isDead);
                EditorGUILayout.LabelField("怪獸死掉過了多久");
                data.deathTimer = EditorGUILayout.FloatField("deathTimer", data.deathTimer);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 距離
            //距離
            foldRange = EditorGUILayout.Foldout(foldRange, "★★★★各種距離設定★★★★");
            if (foldRange)
            {
                EditorGUILayout.LabelField("敵人的警戒範圍的距離 (黃色線)");
                data.float_checkInSight = EditorGUILayout.FloatField("float_checkInSight", data.float_checkInSight);
                EditorGUILayout.LabelField("進入攻擊狀態的範圍 (近戰、遠程皆有用到) (紅色線)");
                data.float_AttackRange = EditorGUILayout.FloatField("float_AttackRange", data.float_AttackRange);
                EditorGUILayout.LabelField("壞人凱爾搜尋好人凱爾的範圍 (紫色線)");
                data.f_BadKyleSearchNiceKyleDistance = EditorGUILayout.FloatField("f_BadKyleSearchNiceKyleDistance", data.f_BadKyleSearchNiceKyleDistance);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 追擊移動
            //追擊移動
            foldChase = EditorGUILayout.Foldout(foldChase, "★★★★各種追擊設定★★★★");
            if (foldChase)
            {
                EditorGUILayout.LabelField("追擊的走路速度，計算用");
                data.Chase_speed = EditorGUILayout.FloatField("Chase_speed", data.Chase_speed);
                EditorGUILayout.LabelField("凱爾的追擊速度，設定用");
                data.kyleSpeed = EditorGUILayout.FloatField("kyleSpeed", data.kyleSpeed);
                EditorGUILayout.LabelField("視角範圍");
                data.SightRange = EditorGUILayout.FloatField("SightRange", data.SightRange);
                EditorGUILayout.LabelField("面對玩家的轉動速度，計算用");
                data.Rotationspeed = EditorGUILayout.FloatField("Rotationspeed", data.Rotationspeed);
                EditorGUILayout.LabelField("凱爾追擊時的轉動速度，設定用");
                data.kyleRotation = EditorGUILayout.FloatField("Rotationspeed", data.kyleRotation);
                EditorGUILayout.LabelField("往前的速度、向量，監看用");
                data.movementVec = EditorGUILayout.Vector3Field("movementVec", data.movementVec);
                EditorGUILayout.LabelField("往前的速度，與玩家之間的向量，監看用");
                data.dirToPlayer = EditorGUILayout.Vector3Field("dirToPlayer", data.dirToPlayer);
                EditorGUILayout.LabelField("是不是在追擊");
                data.isChase = EditorGUILayout.Toggle("isChase", data.isChase);
                EditorGUILayout.LabelField("判斷障礙物的射線長度");
                data.rayDistance = EditorGUILayout.FloatField("rayDistance", data.rayDistance);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 攻擊
            //攻擊
            foldAttack = EditorGUILayout.Foldout(foldAttack, "★★★★各種攻擊設定★★★★");
            if (foldAttack)
            {
                EditorGUILayout.LabelField("是不是進入攻擊狀態");
                data.bool_Attack = EditorGUILayout.Toggle("bool_Attack", data.bool_Attack);
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("有沒有近戰攻擊屬性");
                data.bool_EnemyHasMelleAttackProperty = EditorGUILayout.Toggle("bool_EnemyHasMelleAttackProperty", data.bool_EnemyHasMelleAttackProperty);
                EditorGUILayout.LabelField("是不是進入到近戰攻擊範圍了");
                data.bool_InAttackRange = EditorGUILayout.Toggle("bool_InAttackRange", data.bool_InAttackRange);

                EditorGUILayout.LabelField("進入近戰攻擊範圍後，瞄準主角的旋轉速度");
                data.float_LookRotationSpeed = EditorGUILayout.FloatField("float_LookRotationSpeed", data.float_LookRotationSpeed);

                //下面是畫一個陣列的方法
                EditorGUILayout.BeginVertical(GUI.skin.box);
                EditorGUILayout.LabelField("攻擊的動畫陣列");
                serializedObject.Update();
                SerializedProperty data_MelleAttackAnim = serializedObject.FindProperty("MelleAttackAnim");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_MelleAttackAnim, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.LabelField("凱爾進入狂暴狀態的血量比例");
                data.f_KyleCrazyHealthRate = EditorGUILayout.Slider("f_KyleCrazyHealthRate", data.f_KyleCrazyHealthRate, 0.0f, 0.5f);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 計時器
            //計時器
            foldTimer = EditorGUILayout.Foldout(foldTimer, "★★★★各種時間設定及監看★★★★");
            if (foldTimer)
            {
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                EditorGUILayout.LabelField("下次追擊的計時器");
                data.float_nextChase = EditorGUILayout.FloatField("float_nextChase", data.float_nextChase);
                EditorGUILayout.LabelField("下次追擊的時間設定");
                data.float_chaseRate = EditorGUILayout.FloatField("float_chaseRate", data.float_chaseRate);
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                EditorGUILayout.LabelField("下次攻擊的計時器");
                data.float_NextMelleAttack = EditorGUILayout.FloatField("float_NextMelleAttack", data.float_NextMelleAttack);
                EditorGUILayout.LabelField("下次攻擊的時間設定");
                data.float_MelleAttackRate = EditorGUILayout.FloatField("float_MelleAttackRate", data.float_MelleAttackRate);
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                EditorGUILayout.LabelField("狂暴狀態的計時器");
                data.f_KyleCrazyTimer = EditorGUILayout.FloatField("f_KyleCrazyTimer", data.f_KyleCrazyTimer);
                EditorGUILayout.LabelField("凱爾要在狂暴狀態待多久的時間設定 (程式內有指定)");
                data.f_KyleCrazyTimeSetting = EditorGUILayout.FloatField("f_KyleCrazyTimeSetting", data.f_KyleCrazyTimeSetting);
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 暴打好人
            //暴打好人
            foldBully = EditorGUILayout.Foldout(foldTimer, "★★★★暴打好人凱爾★★★★");
            if (foldBully)
            {
                EditorGUILayout.BeginVertical(GUI.skin.box);
                EditorGUILayout.LabelField("存好人凱爾的陣列");
                serializedObject.Update();
                SerializedProperty data_niceKyles = serializedObject.FindProperty("niceKyles");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_niceKyles, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.LabelField("最近的好人凱爾");
                data.go_nearestNiceKyleToBadKyle = (GameObject)EditorGUILayout.ObjectField("go_nearestNiceKyleToBadKyle", data.go_nearestNiceKyleToBadKyle, typeof(GameObject), true);
                EditorGUILayout.LabelField("有沒有找到好人凱爾");
                data.b_badKyleFindNiceKyle = EditorGUILayout.Toggle("b_badKyleFindNiceKyle", data.b_badKyleFindNiceKyle);
                EditorGUILayout.LabelField("離最近的好人凱爾的距離");
                data.f_DistanceOfNearestNiceKyleToBadKyle = EditorGUILayout.FloatField("f_DistanceOfNearestNiceKyleToBadKyle", data.f_DistanceOfNearestNiceKyleToBadKyle);
                EditorGUILayout.LabelField("壞人凱爾被玩家打了（離開毆打好人狀態）");
                data.b_BadKyleHitByPlayer = EditorGUILayout.Toggle("b_BadKyleHitByPlayer", data.b_BadKyleHitByPlayer);
                EditorGUILayout.LabelField("壞人凱爾毆打好人的速度");
                data.f_BadKyleAttackNiceKyleSpeed = EditorGUILayout.FloatField("f_BadKyleAttackNiceKyleSpeed", data.f_BadKyleAttackNiceKyleSpeed);
                EditorGUILayout.LabelField("壞人凱爾尋找到好人凱爾的距離（在距離那邊也可以調整）");
                data.f_BadKyleSearchNiceKyleDistance = EditorGUILayout.FloatField("f_BadKyleSearchNiceKyleDistance", data.f_BadKyleSearchNiceKyleDistance);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion
            EditorUtility.SetDirty(data);
        }
        #endregion

        #region 長腳
        if (longLegStatus)
        {
            #region 狀態
            //狀態
            GUI.backgroundColor = new Color(250f / 255f, 250f / 255f, 210f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.window);
            EditorGUILayout.LabelField("準備要前往的狀態");
            data.i_LongLegStateNumber = EditorGUILayout.IntField("i_LongLegStateNumber", data.i_LongLegStateNumber);
            GUI.backgroundColor = new Color(173f / 255f, 255f / 255f, 47f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("0 → 待機");
            EditorGUILayout.LabelField("1 → 追逐");
            EditorGUILayout.LabelField("2 → 攻擊");
            EditorGUILayout.LabelField("3 → 後退");
            EditorGUILayout.LabelField("-1 → Null");
            EditorGUILayout.LabelField("無編號 → 死亡");
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            #endregion

            #region 血量
            //血量
            foldHP = EditorGUILayout.Foldout(foldHP, "★★★★血量設定★★★★");
            if (foldHP)
            {
                EditorGUILayout.LabelField("怪獸的最大血量");
                data.maxHp = EditorGUILayout.FloatField("maxHp", data.maxHp);
                EditorGUILayout.LabelField("怪獸現在的血量");
                data.currentHp = EditorGUILayout.FloatField("currentHp", data.currentHp);
                EditorGUILayout.LabelField("怪獸是不是死了");
                data.isDead = EditorGUILayout.Toggle("isDead", data.isDead);
                EditorGUILayout.LabelField("怪獸死掉過了多久");
                data.deathTimer = EditorGUILayout.FloatField("deathTimer", data.deathTimer);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 距離
            //距離
            foldRange = EditorGUILayout.Foldout(foldRange, "★★★★各種距離設定★★★★");
            if (foldRange)
            {
                EditorGUILayout.LabelField("敵人的警戒範圍的距離 (黃色線)");
                data.float_checkInSight = EditorGUILayout.FloatField("float_checkInSight", data.float_checkInSight);
                EditorGUILayout.LabelField("進入攻擊狀態的範圍 (近戰、遠程皆有用到) (紅色線)");
                data.float_AttackRange = EditorGUILayout.FloatField("float_AttackRange", data.float_AttackRange);
                EditorGUILayout.LabelField("要進入後退狀態的範圍  (青色線)");
                data.backRang = EditorGUILayout.FloatField("backRang", data.backRang);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 追擊移動
            //追擊移動
            foldChase = EditorGUILayout.Foldout(foldChase, "★★★★各種追擊設定★★★★");
            if (foldChase)
            {
                EditorGUILayout.LabelField("追擊的走路速度，計算用");
                data.Chase_speed = EditorGUILayout.FloatField("Chase_speed", data.Chase_speed);
                EditorGUILayout.LabelField("追擊速度，設定用");
                data.LongLegSpeed = EditorGUILayout.FloatField("LongLegSpeed", data.LongLegSpeed);
                EditorGUILayout.LabelField("視角範圍");
                data.SightRange = EditorGUILayout.FloatField("SightRange", data.SightRange);
                EditorGUILayout.LabelField("面對玩家的轉動速度，計算用");
                data.Rotationspeed = EditorGUILayout.FloatField("Rotationspeed", data.Rotationspeed);
                EditorGUILayout.LabelField("往前的速度、向量，監看用");
                data.movementVec = EditorGUILayout.Vector3Field("movementVec", data.movementVec);
                EditorGUILayout.LabelField("往前的速度，與玩家之間的向量，監看用");
                data.dirToPlayer = EditorGUILayout.Vector3Field("dirToPlayer", data.dirToPlayer);
                EditorGUILayout.LabelField("是不是在追擊");
                data.isChase = EditorGUILayout.Toggle("isChase", data.isChase);
                EditorGUILayout.LabelField("判斷障礙物的射線長度");
                data.rayDistance = EditorGUILayout.FloatField("rayDistance", data.rayDistance);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 攻擊
            //攻擊
            foldAttack = EditorGUILayout.Foldout(foldAttack, "★★★★各種攻擊設定★★★★");
            if (foldAttack)
            {
                EditorGUILayout.LabelField("是不是進入攻擊狀態");
                data.bool_Attack = EditorGUILayout.Toggle("bool_Attack", data.bool_Attack);
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("有沒有近戰攻擊屬性");
                data.bool_EnemyHasMelleAttackProperty = EditorGUILayout.Toggle("bool_EnemyHasMelleAttackProperty", data.bool_EnemyHasMelleAttackProperty);
                EditorGUILayout.LabelField("是不是進入到近戰攻擊範圍了");
                data.bool_InAttackRange = EditorGUILayout.Toggle("bool_InAttackRange", data.bool_InAttackRange);

                EditorGUILayout.LabelField("進入近戰攻擊範圍後，瞄準主角的旋轉速度");
                data.float_LookRotationSpeed = EditorGUILayout.FloatField("float_LookRotationSpeed", data.float_LookRotationSpeed);

                EditorUtility.SetDirty(data);

                //下面是畫一個陣列的方法
                EditorGUILayout.BeginVertical(GUI.skin.box);
                EditorGUILayout.LabelField("攻擊的動畫陣列");
                serializedObject.Update();
                SerializedProperty data_MelleAttackAnim = serializedObject.FindProperty("MelleAttackAnim");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_MelleAttackAnim, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }
                EditorGUILayout.EndHorizontal();
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 計時器
            //計時器
            foldTimer = EditorGUILayout.Foldout(foldTimer, "★★★★各種時間設定及監看★★★★");
            if (foldTimer)
            {
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                EditorGUILayout.LabelField("下次追擊的計時器");
                data.float_nextChase = EditorGUILayout.FloatField("float_nextChase", data.float_nextChase);
                EditorGUILayout.LabelField("下次追擊的時間設定");
                data.float_chaseRate = EditorGUILayout.FloatField("float_chaseRate", data.float_chaseRate);
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                EditorGUILayout.LabelField("下次攻擊的計時器");
                data.float_NextMelleAttack = EditorGUILayout.FloatField("float_NextMelleAttack", data.float_NextMelleAttack);
                EditorGUILayout.LabelField("下次攻擊的時間設定");
                data.float_MelleAttackRate = EditorGUILayout.FloatField("float_MelleAttackRate", data.float_MelleAttackRate);
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                EditorGUILayout.LabelField("後退狀態的計時器");
                data.backWaitTimerMelle = EditorGUILayout.FloatField("backWaitTimerMelle", data.backWaitTimerMelle);
                EditorGUILayout.LabelField("後退狀態要當多久的時間設定");
                data.backWaitTimeMelleSetting = EditorGUILayout.FloatField("backWaitTimeMelleSetting", data.backWaitTimeMelleSetting);
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                EditorGUILayout.LabelField("可以再次回到後退狀態的計時器");
                data.LongLegCanBackAgainTimer = EditorGUILayout.FloatField("LongLegCanBackAgainTimer", data.LongLegCanBackAgainTimer);
                EditorGUILayout.LabelField("可以再次後退的時間設定");
                data.LongLegCanBackAgainTimeSetting = EditorGUILayout.FloatField("LongLegCanBackAgainTimeSetting", data.LongLegCanBackAgainTimeSetting);
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 後退
            //後退
            foldBack = EditorGUILayout.Foldout(foldBack, "★★★★各種後退數值設定★★★★");
            if (foldBack)
            {
                EditorGUILayout.LabelField("是不是在後退範圍");
                data.inBackRange = EditorGUILayout.Toggle("inBackRange", data.inBackRange);
                EditorGUILayout.LabelField("是不是在後退狀態");
                data.isBack = EditorGUILayout.Toggle("isBack", data.isBack);
                EditorGUILayout.LabelField("往後退的走路速度，監看用");
                data.backSpeed = EditorGUILayout.FloatField("backSpeed", data.backSpeed);
                EditorGUILayout.LabelField("長腳機器人往後的速度，設定用 (要負的)");
                data.backSpeed_LongLeg = EditorGUILayout.FloatField("backSpeed_LongLeg", data.backSpeed_LongLeg);
                EditorGUILayout.LabelField("進入往後退狀態的範圍");
                data.backRang = EditorGUILayout.FloatField("backRang", data.backRang);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion
            EditorUtility.SetDirty(data);
        }
        #endregion

        #region 泡泡蜘蛛機器人
        if (spiderStatus)
        {
            #region 狀態
            //狀態
            GUI.backgroundColor = new Color(250f / 255f, 250f / 255f, 210f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.window);
            EditorGUILayout.LabelField("準備要前往的狀態");
            data.i_SpiderStateNumber = EditorGUILayout.IntField("i_SpiderStateNumber", data.i_SpiderStateNumber);
            GUI.backgroundColor = new Color(173f / 255f, 255f / 255f, 47f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("0 → 待機");
            EditorGUILayout.LabelField("1 → 追逐");
            EditorGUILayout.LabelField("2 → 攻擊");
            EditorGUILayout.LabelField("3 → 射泡泡");
            EditorGUILayout.LabelField("-1 → Null");
            EditorGUILayout.LabelField("無編號 → 死亡");
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            #endregion

            #region 血量
            //血量
            foldHP = EditorGUILayout.Foldout(foldHP, "★★★★血量設定★★★★");
            if (foldHP)
            {
                EditorGUILayout.LabelField("怪獸的最大血量");
                data.maxHp = EditorGUILayout.FloatField("maxHp", data.maxHp);
                EditorGUILayout.LabelField("怪獸現在的血量");
                data.currentHp = EditorGUILayout.FloatField("currentHp", data.currentHp);
                EditorGUILayout.LabelField("怪獸是不是死了");
                data.isDead = EditorGUILayout.Toggle("isDead", data.isDead);
                EditorGUILayout.LabelField("怪獸死掉過了多久");
                data.deathTimer = EditorGUILayout.FloatField("deathTimer", data.deathTimer);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 距離
            //距離
            foldRange = EditorGUILayout.Foldout(foldRange, "★★★★各種距離設定★★★★");
            if (foldRange)
            {
                EditorGUILayout.LabelField("敵人的警戒範圍的距離 (黃色線)");
                data.float_checkInSight = EditorGUILayout.FloatField("float_checkInSight", data.float_checkInSight);
                EditorGUILayout.LabelField("進入攻擊狀態的範圍 (近戰、遠程皆有用到) (紅色線)");
                data.float_AttackRange = EditorGUILayout.FloatField("float_AttackRange", data.float_AttackRange);
                EditorGUILayout.LabelField("要進入後退狀態的範圍  (青色線)");
                data.backRang = EditorGUILayout.FloatField("backRang", data.backRang);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 射擊
            //射擊
            foldShoot = EditorGUILayout.Foldout(foldShoot, "★★★★★ 射擊設定 ★★★★★");
            if (foldShoot)
            {
                EditorGUILayout.LabelField("是否有遠攻屬性");
                data.bool_EnemyHasRemoteAttackProperty = EditorGUILayout.Toggle("bool_EnemyHasRemoteAttackProperty", data.bool_EnemyHasRemoteAttackProperty);

                EditorGUILayout.LabelField("蜘蛛機器人的炮台");
                data.go_spiderTurret = (GameObject)EditorGUILayout.ObjectField("go_spiderTurret", data.go_spiderTurret, typeof(GameObject), true);
                EditorGUILayout.LabelField("蜘蛛機器人砲台上的發射口");
                data.go_spiderTurretEmitter = (GameObject)EditorGUILayout.ObjectField("go_spiderTurretEmitter", data.go_spiderTurretEmitter, typeof(GameObject), true);
                EditorGUILayout.LabelField("蜘蛛機器人砲台的旋轉速度");
                data.f_spiderTurretRotationSpeed = EditorGUILayout.FloatField("f_spiderTurretRotationSpeed", data.f_spiderTurretRotationSpeed);
                EditorGUILayout.LabelField("蜘蛛機器人瞄準角度的補助用向量");
                data.v_spiderAimingAngle = EditorGUILayout.Vector3Field("v_spiderAimingAngle", data.v_spiderAimingAngle);

                EditorGUILayout.Space();
                EditorGUILayout.LabelField("泡泡射擊速度，數字越大越慢(時間是倒數的)");
                data.float_BubbleShootSpeed = EditorGUILayout.FloatField("float_BubbleShootSpeed", data.float_BubbleShootSpeed);
                EditorUtility.SetDirty(data);
                //EditorGUILayout.LabelField("第一次射泡泡的延遲時間");
                //data.float_FirstShootDelayTime = EditorGUILayout.FloatField("float_FirstShootDelayTime", data.float_FirstShootDelayTime);
                EditorGUILayout.LabelField("一波泡泡射擊總時間");
                data.float_WaveShootTotalTime = EditorGUILayout.FloatField("float_WaveShootTotalTime", data.float_WaveShootTotalTime);
                EditorGUILayout.LabelField("泡泡射擊完之後的Delay時間，Delay完之後若條件達成繼續下一波");
                data.float_WaveShootOverDelayTime = EditorGUILayout.FloatField("float_WaveShootOverDelayTime", data.float_WaveShootOverDelayTime);
                EditorUtility.SetDirty(data);

                //下面是顯示陣列的方法
                EditorGUILayout.LabelField("泡泡的發射口群組");
                serializedObject.Update();
                SerializedProperty data_transArray_Emiiter = serializedObject.FindProperty("transArray_Emiiter");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_transArray_Emiiter, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 計時器
            //計時器
            foldTimer = EditorGUILayout.Foldout(foldTimer, "★★★★各種時間設定及監看★★★★");
            if (foldTimer)
            {
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                EditorGUILayout.LabelField("可以在射擊狀態待多久的計時器");
                data.shootingToWalkingTimer = EditorGUILayout.FloatField("shootingToWalkingTimer", data.shootingToWalkingTimer);
                EditorGUILayout.LabelField("可以在射擊狀態待多久的時間設定");
                data.shootingToWalkingTimeSetting = EditorGUILayout.FloatField("shootingToWalkingTimeSetting", data.shootingToWalkingTimeSetting);
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                //GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                //EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                //EditorGUILayout.LabelField("下次再攻擊的計時器");
                //data.shootingToWalkingTimeSetting = EditorGUILayout.FloatField("shootingToWalkingTimeSetting", data.shootingToWalkingTimeSetting);
                //EditorGUILayout.LabelField("下次再射擊的時間設定");
                //data.shootingToWalkingTimer = EditorGUILayout.FloatField("shootingToWalkingTimer", data.shootingToWalkingTimer);
                //GUI.backgroundColor = Color.white;
                //EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                EditorGUILayout.LabelField("可以再次回到後退狀態的計時器");
                data.f_SpiderCanBackAgainTimer = EditorGUILayout.FloatField("f_SpiderCanBackAgainTimer", data.f_SpiderCanBackAgainTimer);
                EditorGUILayout.LabelField("可以再次後退的時間設定");
                data.f_SpiderCanBackAgainTimeSetting = EditorGUILayout.FloatField("f_SpiderCanBackAgainTimeSetting", data.f_SpiderCanBackAgainTimeSetting);
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                EditorGUILayout.LabelField("後退狀態的計時器");
                data.backWaitTimer = EditorGUILayout.FloatField("backWaitTimer", data.backWaitTimer);
                EditorGUILayout.LabelField("後退狀態要當多久的時間設定");
                data.backWaitTimeMelleSetting = EditorGUILayout.FloatField("backWaitTimeMelleSetting", data.backWaitTimeMelleSetting);
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 後退
            //後退
            foldBack = EditorGUILayout.Foldout(foldBack, "★★★★各種後退數值設定★★★★");
            if (foldBack)
            {
                EditorGUILayout.LabelField("是不是在後退範圍");
                data.inBackRange = EditorGUILayout.Toggle("inBackRange", data.inBackRange);
                EditorGUILayout.LabelField("是不是在後退狀態");
                data.isBack = EditorGUILayout.Toggle("isBack", data.isBack);
                EditorGUILayout.LabelField("往後退的走路速度，監看用");
                data.backSpeed = EditorGUILayout.FloatField("backSpeed", data.backSpeed);
                EditorGUILayout.LabelField("蜘蛛機器人往後的速度，設定用 (要負的)");
                data.backSpeed_Spider = EditorGUILayout.FloatField("backSpeed_Spider", data.backSpeed_Spider);
                EditorGUILayout.LabelField("進入往後退狀態的範圍");
                data.backRang = EditorGUILayout.FloatField("backRang", data.backRang);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion 
            EditorUtility.SetDirty(data);
        }
        EditorGUILayout.Space();


        #endregion

        #region 雷射蜘蛛機器人
        if (laserSpiderStatus)
        {
            #region 狀態
            //狀態
            GUI.backgroundColor = new Color(250f / 255f, 250f / 255f, 210f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.window);
            EditorGUILayout.LabelField("準備要前往的狀態");
            data.i_LaserSpiderStateNumber = EditorGUILayout.IntField("i_LaserSpiderStateNumber", data.i_LaserSpiderStateNumber);
            GUI.backgroundColor = new Color(173f / 255f, 255f / 255f, 47f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("0 → 待機");
            EditorGUILayout.LabelField("1 → 追逐");
            EditorGUILayout.LabelField("2 → 攻擊");
            EditorGUILayout.LabelField("3 → 射雷射");
            EditorGUILayout.LabelField("-1 → Null");
            EditorGUILayout.LabelField("無編號 → 死亡");
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            #endregion

            #region 血量
            //血量
            foldHP = EditorGUILayout.Foldout(foldHP, "★★★★血量設定★★★★");
            if (foldHP)
            {
                EditorGUILayout.LabelField("怪獸的最大血量");
                data.maxHp = EditorGUILayout.FloatField("maxHp", data.maxHp);
                EditorGUILayout.LabelField("怪獸現在的血量");
                data.currentHp = EditorGUILayout.FloatField("currentHp", data.currentHp);
                EditorGUILayout.LabelField("怪獸是不是死了");
                data.isDead = EditorGUILayout.Toggle("isDead", data.isDead);
                EditorGUILayout.LabelField("怪獸死掉過了多久");
                data.deathTimer = EditorGUILayout.FloatField("deathTimer", data.deathTimer);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 距離
            //距離
            foldRange = EditorGUILayout.Foldout(foldRange, "★★★★各種距離設定★★★★");
            if (foldRange)
            {
                EditorGUILayout.LabelField("敵人的警戒範圍的距離 (黃色線)");
                data.float_checkInSight = EditorGUILayout.FloatField("float_checkInSight", data.float_checkInSight);
                EditorGUILayout.LabelField("進入攻擊狀態的範圍 (近戰、遠程皆有用到) (紅色線)");
                data.float_AttackRange = EditorGUILayout.FloatField("float_AttackRange", data.float_AttackRange);
                EditorGUILayout.LabelField("要進入後退狀態的範圍  (青色線)");
                data.backRang = EditorGUILayout.FloatField("backRang", data.backRang);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 射擊
            //射擊
            foldShoot = EditorGUILayout.Foldout(foldShoot, "★★★★★ 射擊設定 ★★★★★");
            if (foldShoot)
            {
                EditorGUILayout.LabelField("是否有遠攻屬性");
                data.bool_EnemyHasRemoteAttackProperty = EditorGUILayout.Toggle("bool_EnemyHasRemoteAttackProperty", data.bool_EnemyHasRemoteAttackProperty);

                EditorGUILayout.LabelField("蜘蛛機器人的炮台");
                data.go_spiderTurret = (GameObject)EditorGUILayout.ObjectField("go_spiderTurret", data.go_spiderTurret, typeof(GameObject), true);
                EditorGUILayout.LabelField("蜘蛛機器人砲台上的發射口");
                data.go_spiderTurretEmitter = (GameObject)EditorGUILayout.ObjectField("go_spiderTurretEmitter", data.go_spiderTurretEmitter, typeof(GameObject), true);
                EditorGUILayout.LabelField("蜘蛛機器人砲台的旋轉速度");
                data.f_spiderTurretRotationSpeed = EditorGUILayout.FloatField("f_spiderTurretRotationSpeed", data.f_spiderTurretRotationSpeed);
                EditorGUILayout.LabelField("蜘蛛機器人瞄準角度的補助用向量");
                data.v_spiderAimingAngle = EditorGUILayout.Vector3Field("v_spiderAimingAngle", data.v_spiderAimingAngle);

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("雷射蜘蛛機器人的雷射物件");
                data.go_LaserParent = (GameObject)EditorGUILayout.ObjectField("go_LaserParent", data.go_LaserParent, typeof(GameObject), true);
                EditorGUILayout.LabelField("雷射蜘蛛機器人的雷射啟動光束");
                data.go_ChargeLaser = (GameObject)EditorGUILayout.ObjectField("go_ChargeLaser", data.go_ChargeLaser, typeof(GameObject), true);
                EditorGUILayout.LabelField("雷射蜘蛛機器人的雷射最大威力光束");
                data.go_BeamLaser = (GameObject)EditorGUILayout.ObjectField("go_BeamLaser", data.go_BeamLaser, typeof(GameObject), true);
                EditorGUILayout.LabelField("雷射蜘蛛機器人的雷射發射口的煙和粒子物件");
                data.go_SmokeAndSparks = (GameObject)EditorGUILayout.ObjectField("go_SmokeAndSparks", data.go_SmokeAndSparks, typeof(GameObject), true);
                EditorGUILayout.LabelField("雷射蜘蛛機器人的雷射結束光束");
                data.go_EndLaser = (GameObject)EditorGUILayout.ObjectField("go_EndLaser", data.go_EndLaser, typeof(GameObject), true);
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("雷射發射的計時器，Function裡面用\n控制三種雷射啟動→最大→結束用");
                data.f_LaserTimer = EditorGUILayout.FloatField("f_LaserTimer", data.f_LaserTimer);


                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 計時器
            //計時器
            foldTimer = EditorGUILayout.Foldout(foldTimer, "★★★★各種時間設定及監看★★★★");
            if (foldTimer)
            {
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                EditorGUILayout.LabelField("可以在射擊狀態待多久的計時器");
                data.shootingToWalkingTimer = EditorGUILayout.FloatField("shootingToWalkingTimer", data.shootingToWalkingTimer);
                EditorGUILayout.LabelField("可以在射擊狀態待多久的時間設定");
                data.shootingToWalkingTimeSetting = EditorGUILayout.FloatField("shootingToWalkingTimeSetting", data.shootingToWalkingTimeSetting);
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                

                GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                EditorGUILayout.LabelField("可以再次回到後退狀態的計時器");
                data.f_SpiderCanBackAgainTimer = EditorGUILayout.FloatField("f_SpiderCanBackAgainTimer", data.f_SpiderCanBackAgainTimer);
                EditorGUILayout.LabelField("可以再次後退的時間設定");
                data.f_SpiderCanBackAgainTimeSetting = EditorGUILayout.FloatField("f_SpiderCanBackAgainTimeSetting", data.f_SpiderCanBackAgainTimeSetting);
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                EditorGUILayout.LabelField("後退狀態的計時器");
                data.backWaitTimer = EditorGUILayout.FloatField("backWaitTimer", data.backWaitTimer);
                EditorGUILayout.LabelField("後退狀態要當多久的時間設定");
                data.backWaitTimeMelleSetting = EditorGUILayout.FloatField("backWaitTimeMelleSetting", data.backWaitTimeMelleSetting);
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                EditorGUILayout.LabelField("雷射充能的計時器");
                data.f_LaserRechargeTimer = EditorGUILayout.FloatField("f_LaserRechargeTimer", data.f_LaserRechargeTimer);
                EditorGUILayout.LabelField("雷射充能要充多久的時間設定");
                data.f_LaserRechargeTimeSetting = EditorGUILayout.FloatField("f_LaserRechargeTimeSetting", data.f_LaserRechargeTimeSetting);
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 後退
            //後退
            foldBack = EditorGUILayout.Foldout(foldBack, "★★★★各種後退數值設定★★★★");
            if (foldBack)
            {
                EditorGUILayout.LabelField("是不是在後退範圍");
                data.inBackRange = EditorGUILayout.Toggle("inBackRange", data.inBackRange);
                EditorGUILayout.LabelField("是不是在後退狀態");
                data.isBack = EditorGUILayout.Toggle("isBack", data.isBack);
                EditorGUILayout.LabelField("往後退的走路速度，監看用");
                data.backSpeed = EditorGUILayout.FloatField("backSpeed", data.backSpeed);
                EditorGUILayout.LabelField("蜘蛛機器人往後的速度，設定用 (要負的)");
                data.backSpeed_Spider = EditorGUILayout.FloatField("backSpeed_Spider", data.backSpeed_Spider);
                EditorGUILayout.LabelField("進入往後退狀態的範圍");
                data.backRang = EditorGUILayout.FloatField("backRang", data.backRang);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion
            EditorUtility.SetDirty(data);
        }
        #endregion

        #region 凱爾隊長
        if (kyleWallStatus)
        {
            #region 狀態
            //狀態
            GUI.backgroundColor = new Color(250f / 255f, 250f / 255f, 210f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.window);
            EditorGUILayout.LabelField("準備要前往的狀態");
            data.i_KyleWallStateNumber = EditorGUILayout.IntField("i_KyleWallStateNumber", data.i_KyleWallStateNumber);
            GUI.backgroundColor = new Color(173f / 255f, 255f / 255f, 47f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("0 → 待機");
            EditorGUILayout.LabelField("1 → 攻擊");            
            EditorGUILayout.LabelField("-1 → Null");
            EditorGUILayout.LabelField("無編號 → 死亡");
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            #endregion

            #region 血量
            //血量
            foldHP = EditorGUILayout.Foldout(foldHP, "★★★★血量設定★★★★");
            if (foldHP)
            {
                EditorGUILayout.LabelField("怪獸的最大血量");
                data.maxHp = EditorGUILayout.FloatField("maxHp", data.maxHp);
                EditorGUILayout.LabelField("怪獸現在的血量");
                data.currentHp = EditorGUILayout.FloatField("currentHp", data.currentHp);
                EditorGUILayout.LabelField("怪獸是不是死了");
                data.isDead = EditorGUILayout.Toggle("isDead", data.isDead);
                EditorGUILayout.LabelField("怪獸死掉過了多久");
                data.deathTimer = EditorGUILayout.FloatField("deathTimer", data.deathTimer);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 距離
            //距離
            foldRange = EditorGUILayout.Foldout(foldRange, "★★★★各種距離設定★★★★");
            if (foldRange)
            {
                EditorGUILayout.LabelField("敵人的警戒範圍的距離 (黃色線)");
                data.float_checkInSight = EditorGUILayout.FloatField("float_checkInSight", data.float_checkInSight);
                EditorGUILayout.LabelField("進入攻擊狀態的範圍 (近戰、遠程皆有用到) (紅色線)");
                data.float_AttackRange = EditorGUILayout.FloatField("float_AttackRange", data.float_AttackRange);
                //EditorGUILayout.LabelField("要進入後退狀態的範圍  (青色線)");
                //data.backRang = EditorGUILayout.FloatField("backRang", data.backRang);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 攻擊
            //攻擊
            foldAttack = EditorGUILayout.Foldout(foldAttack, "★★★★各種攻擊設定★★★★");
            if (foldAttack)
            {
                EditorGUILayout.LabelField("是不是進入攻擊狀態");
                data.bool_Attack = EditorGUILayout.Toggle("bool_Attack", data.bool_Attack);
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("有沒有近戰攻擊屬性");
                data.bool_EnemyHasMelleAttackProperty = EditorGUILayout.Toggle("bool_EnemyHasMelleAttackProperty", data.bool_EnemyHasMelleAttackProperty);
                EditorGUILayout.LabelField("是不是進入到近戰攻擊範圍了");
                data.bool_InAttackRange = EditorGUILayout.Toggle("bool_InAttackRange", data.bool_InAttackRange);

                EditorGUILayout.LabelField("瞄準主角的旋轉速度");
                data.Rotationspeed = EditorGUILayout.FloatField("Rotationspeed", data.Rotationspeed);

                ////下面是畫一個陣列的方法
                //EditorGUILayout.BeginVertical(GUI.skin.box);
                //EditorGUILayout.LabelField("攻擊的動畫陣列");
                //serializedObject.Update();
                //SerializedProperty data_MelleAttackAnim = serializedObject.FindProperty("MelleAttackAnim");
                //EditorGUI.BeginChangeCheck();
                //EditorGUILayout.PropertyField(data_MelleAttackAnim, true);
                //if (EditorGUI.EndChangeCheck())
                //{
                //    serializedObject.ApplyModifiedProperties();
                //}
                //EditorGUILayout.EndHorizontal();

                //EditorGUILayout.LabelField("凱爾進入狂暴狀態的血量比例");
                //data.f_KyleCrazyHealthRate = EditorGUILayout.Slider("f_KyleCrazyHealthRate", data.f_KyleCrazyHealthRate, 0.0f, 0.5f);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 計時器
            //計時器
            foldTimer = EditorGUILayout.Foldout(foldTimer, "★★★★各種時間設定及監看★★★★");
            if (foldTimer)
            {
                //EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                //EditorGUILayout.LabelField("下次追擊的計時器");
                //data.float_nextChase = EditorGUILayout.FloatField("float_nextChase", data.float_nextChase);
                //EditorGUILayout.LabelField("下次追擊的時間設定");
                //data.float_chaseRate = EditorGUILayout.FloatField("float_chaseRate", data.float_chaseRate);
                //EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                EditorGUILayout.LabelField("下次攻擊的計時器");
                data.float_NextMelleAttack = EditorGUILayout.FloatField("float_NextMelleAttack", data.float_NextMelleAttack);
                EditorGUILayout.LabelField("下次攻擊的時間設定");
                data.float_MelleAttackRate = EditorGUILayout.FloatField("float_MelleAttackRate", data.float_MelleAttackRate);
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                //EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                //EditorGUILayout.LabelField("狂暴狀態的計時器");
                //data.f_KyleCrazyTimer = EditorGUILayout.FloatField("f_KyleCrazyTimer", data.f_KyleCrazyTimer);
                //EditorGUILayout.LabelField("凱爾要在狂暴狀態待多久的時間設定 (程式內有指定)");
                //data.f_KyleCrazyTimeSetting = EditorGUILayout.FloatField("f_KyleCrazyTimeSetting", data.f_KyleCrazyTimeSetting);
                //EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion
            EditorUtility.SetDirty(data);
        }
        #endregion

        #region 好人凱爾
        if (niceKyleStatus)
        {
            #region 狀態
            //狀態
            GUI.backgroundColor = new Color(250f / 255f, 250f / 255f, 210f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.window);
            EditorGUILayout.LabelField("準備要前往的狀態");
            data.i_NKStateNumber = EditorGUILayout.IntField("i_NKStateNumber", data.i_NKStateNumber);
            GUI.backgroundColor = new Color(173f / 255f, 255f / 255f, 47f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("0 → 待機");
            EditorGUILayout.LabelField("1 → 講話");
            EditorGUILayout.LabelField("3 → 攻擊待機");
            EditorGUILayout.LabelField("5 → 攻擊");
            EditorGUILayout.LabelField("-1 → Null");
            EditorGUILayout.LabelField("無編號 → 死亡");
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            #endregion

            #region 距離
            //距離
            foldRange = EditorGUILayout.Foldout(foldRange, "★★★★各種距離設定★★★★");
            if (foldRange)
            {
                EditorGUILayout.LabelField("講話的範圍");
                data.f_NKSpeakRange = EditorGUILayout.FloatField("講話的範圍", data.f_NKSpeakRange);
                EditorGUILayout.LabelField("好人凱爾攻擊範圍");
                data.f_NKAttackRange = EditorGUILayout.FloatField("f_NKAttackRange", data.f_NKAttackRange);
                //EditorGUILayout.LabelField("要進入後退狀態的範圍  (青色線)");
                //data.backRang = EditorGUILayout.FloatField("backRang", data.backRang);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 攻擊
            //攻擊
            foldAttack = EditorGUILayout.Foldout(foldAttack, "★★★★各種攻擊設定★★★★");
            if (foldAttack)
            {
                EditorGUILayout.LabelField("是否允許旋轉");
                data.b_NKRotate = EditorGUILayout.Toggle("b_NKRotate", data.b_NKRotate);
                //EditorGUILayout.Space();
                //EditorGUILayout.LabelField("有沒有近戰攻擊屬性");
                //data.bool_EnemyHasMelleAttackProperty = EditorGUILayout.Toggle("bool_EnemyHasMelleAttackProperty", data.bool_EnemyHasMelleAttackProperty);
                //EditorGUILayout.LabelField("是不是進入到近戰攻擊範圍了");
                //data.bool_InAttackRange = EditorGUILayout.Toggle("bool_InAttackRange", data.bool_InAttackRange);

                EditorGUILayout.LabelField("瞄準壞人的旋轉速度");
                data.f_NKRotateSpeed = EditorGUILayout.FloatField("f_NKRotateSpeed", data.f_NKRotateSpeed);

                ////下面是畫一個陣列的方法
                //EditorGUILayout.BeginVertical(GUI.skin.box);
                //EditorGUILayout.LabelField("攻擊的動畫陣列");
                //serializedObject.Update();
                //SerializedProperty data_MelleAttackAnim = serializedObject.FindProperty("MelleAttackAnim");
                //EditorGUI.BeginChangeCheck();
                //EditorGUILayout.PropertyField(data_MelleAttackAnim, true);
                //if (EditorGUI.EndChangeCheck())
                //{
                //    serializedObject.ApplyModifiedProperties();
                //}
                //EditorGUILayout.EndHorizontal();

                //EditorGUILayout.LabelField("凱爾進入狂暴狀態的血量比例");
                //data.f_KyleCrazyHealthRate = EditorGUILayout.Slider("f_KyleCrazyHealthRate", data.f_KyleCrazyHealthRate, 0.0f, 0.5f);


                EditorGUILayout.LabelField("存敵人物件");
                serializedObject.Update();                
                SerializedProperty data_otherEnemies = serializedObject.FindProperty("otherEnemies");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_otherEnemies, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }

                EditorGUILayout.BeginVertical(GUI.skin.box);
                EditorGUILayout.LabelField("是不是在場上說話的凱爾");
                data.b_talkingKyle = EditorGUILayout.Toggle("b_talkingKyle", data.b_talkingKyle);
                EditorGUILayout.LabelField("是不是在場上戰鬥的凱爾");
                data.b_fightingKyle = EditorGUILayout.Toggle("b_fightingKyle", data.b_fightingKyle);
                EditorGUILayout.EndVertical();//畫個盒子結束   

                EditorGUILayout.LabelField("離最近的壞人的距離");
                data.f_DistanceOfNearstEnemyToNiceKyle = EditorGUILayout.FloatField("f_DistanceOfNearstEnemyToNiceKyle", data.f_DistanceOfNearstEnemyToNiceKyle);
                EditorGUILayout.LabelField("是不是在攻擊範圍了");
                data.b_enemiesInNiceKyleAttackRange = EditorGUILayout.Toggle("b_enemiesInNiceKyleAttackRange", data.b_enemiesInNiceKyleAttackRange);
                EditorGUILayout.LabelField("最近的那個壞人");
                data.go_nearestEnemyToNiceKyle = (GameObject)EditorGUILayout.ObjectField("go_nearestEnemyToNiceKyle", data.go_nearestEnemyToNiceKyle, typeof(GameObject), true);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 計時器
            //計時器
            foldTimer = EditorGUILayout.Foldout(foldTimer, "★★★★各種時間設定及監看★★★★");
            if (foldTimer)
            {
                //EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                //EditorGUILayout.LabelField("下次追擊的計時器");
                //data.float_nextChase = EditorGUILayout.FloatField("float_nextChase", data.float_nextChase);
                //EditorGUILayout.LabelField("下次追擊的時間設定");
                //data.float_chaseRate = EditorGUILayout.FloatField("float_chaseRate", data.float_chaseRate);
                //EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                EditorGUILayout.LabelField("下次攻擊的計時器");
                data.f_NKTimer = EditorGUILayout.FloatField("f_NKTimer", data.f_NKTimer);
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                //EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                //EditorGUILayout.LabelField("狂暴狀態的計時器");
                //data.f_KyleCrazyTimer = EditorGUILayout.FloatField("f_KyleCrazyTimer", data.f_KyleCrazyTimer);
                //EditorGUILayout.LabelField("凱爾要在狂暴狀態待多久的時間設定 (程式內有指定)");
                //data.f_KyleCrazyTimeSetting = EditorGUILayout.FloatField("f_KyleCrazyTimeSetting", data.f_KyleCrazyTimeSetting);
                //EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            EditorUtility.SetDirty(data);
        }
        #endregion

        #region 防禦塔
        if (defenseTower)
        {
            #region 狀態
            //狀態
            GUI.backgroundColor = new Color(250f / 255f, 250f / 255f, 210f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.window);
            EditorGUILayout.LabelField("準備要前往的狀態");
            data.i_heavenTurrentState = EditorGUILayout.IntField("i_heavenTurrentState", data.i_heavenTurrentState);
            GUI.backgroundColor = new Color(173f / 255f, 255f / 255f, 47f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("0 → 待機");
            EditorGUILayout.LabelField("1 → 瞄準射擊");
            EditorGUILayout.LabelField("-1 → Null");
            EditorGUILayout.LabelField("無編號 → 死亡");
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            #endregion

            #region 射擊
            //射擊
            foldShoot = EditorGUILayout.Foldout(foldShoot, "★★★★★ 射擊設定 ★★★★★");
            if (foldShoot)
            {
                EditorGUILayout.LabelField("是否有遠攻屬性");
                data.bool_EnemyHasRemoteAttackProperty = EditorGUILayout.Toggle("bool_EnemyHasRemoteAttackProperty", data.bool_EnemyHasRemoteAttackProperty);

                EditorGUILayout.LabelField("要旋轉的點，旋轉的砲口");
                data.go_turrentEmitter = (GameObject)EditorGUILayout.ObjectField("go_turrentEmitter", data.go_turrentEmitter, typeof(GameObject), true);
                //EditorGUILayout.LabelField("蜘蛛機器人砲台上的發射口");
                //data.go_spiderTurretEmitter = (GameObject)EditorGUILayout.ObjectField("go_spiderTurretEmitter", data.go_spiderTurretEmitter, typeof(GameObject), true);
                EditorGUILayout.LabelField("砲台的旋轉速度");
                data.f_heavenTurrentRotationSpeed = EditorGUILayout.FloatField("f_heavenTurrentRotationSpeed", data.f_heavenTurrentRotationSpeed);
                //EditorGUILayout.LabelField("蜘蛛機器人瞄準角度的補助用向量");
                //data.v_spiderAimingAngle = EditorGUILayout.Vector3Field("v_spiderAimingAngle", data.v_spiderAimingAngle);
                EditorGUILayout.LabelField("射擊泡泡的種類，1=不等速，2=等速，3=追擊");
                data.i_heavenTurrentShootPattern = EditorGUILayout.IntField("i_heavenTurrentShootPattern", data.i_heavenTurrentShootPattern);

                EditorGUILayout.Space();
                EditorGUILayout.LabelField("泡泡射擊速度，數字越大越慢(時間是倒數的)");
                data.float_BubbleShootSpeed = EditorGUILayout.FloatField("float_BubbleShootSpeed", data.float_BubbleShootSpeed);
                //EditorGUILayout.LabelField("第一次射泡泡的延遲時間");
                //data.float_FirstShootDelayTime = EditorGUILayout.FloatField("float_FirstShootDelayTime", data.float_FirstShootDelayTime);
                EditorGUILayout.LabelField("一波泡泡射擊總時間");
                data.float_WaveShootTotalTime = EditorGUILayout.FloatField("float_WaveShootTotalTime", data.float_WaveShootTotalTime);
                EditorGUILayout.LabelField("泡泡射擊完之後的Delay時間，Delay完之後若條件達成繼續下一波");
                data.float_WaveShootOverDelayTime = EditorGUILayout.FloatField("float_WaveShootOverDelayTime", data.float_WaveShootOverDelayTime);

                //下面是顯示陣列的方法
                EditorGUILayout.LabelField("泡泡的發射口群組");
                serializedObject.Update();
                SerializedProperty data_tr_turrentEmitter = serializedObject.FindProperty("tr_turrentEmitter");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_tr_turrentEmitter, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion
            EditorUtility.SetDirty(data);
        }
        #endregion

        #region LastBoss
        if (bossStatus)
        {
            #region 狀態
            //狀態
            GUI.backgroundColor = new Color(250f / 255f, 250f / 255f, 210f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.window);
            EditorGUILayout.LabelField("準備要前往的狀態");
            data.i_LBStatePattern = EditorGUILayout.IntField("i_LBStatePattern", data.i_LBStatePattern);
            GUI.backgroundColor = new Color(173f / 255f, 255f / 255f, 47f / 255f);
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.LabelField("8 → 共用待機");
            EditorGUILayout.LabelField("0 → 專用第一次待機");
            EditorGUILayout.LabelField("4 → 專用第一次射擊");
            EditorGUILayout.LabelField("1 → 專用待機");
            EditorGUILayout.LabelField("7 → 跳躍攻擊");
            EditorGUILayout.LabelField("2 → 往前追擊");
            EditorGUILayout.LabelField("6 → 近戰攻擊 (內含隨機抽跳躍)");
            EditorGUILayout.LabelField("5 → 遠程攻擊");
            EditorGUILayout.LabelField("-1 → Null");
            EditorGUILayout.LabelField("9 → 專用死亡");
            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();
            #endregion

            #region 血量
            //血量
            foldHP = EditorGUILayout.Foldout(foldHP, "★★★★血量設定★★★★");
            if (foldHP)
            {
                EditorGUILayout.LabelField("怪獸的最大血量");
                data.maxHp = EditorGUILayout.FloatField("maxHp", data.maxHp);
                EditorGUILayout.LabelField("怪獸現在的血量");
                data.currentHp = EditorGUILayout.FloatField("currentHp", data.currentHp);
                EditorGUILayout.LabelField("怪獸是不是死了");
                data.isDead = EditorGUILayout.Toggle("isDead", data.isDead);
                EditorGUILayout.LabelField("怪獸死掉過了多久");
                data.deathTimer = EditorGUILayout.FloatField("deathTimer", data.deathTimer);
                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 距離
            //距離
            foldRange = EditorGUILayout.Foldout(foldRange, "★★★★各種距離設定★★★★");
            if (foldRange)
            {
                EditorGUILayout.LabelField("敵人的警戒範圍的距離 (黃色線)，進入範圍後起動魔王所有狀態");
                data.float_checkInSight = EditorGUILayout.FloatField("float_checkInSight", data.float_checkInSight);                
                EditorGUILayout.LabelField("近戰只砍一下的動作之距離設定");
                data.f_LBMelleAttackRange1 = EditorGUILayout.FloatField("f_LBMelleAttackRange1", data.f_LBMelleAttackRange1);
                EditorGUILayout.LabelField("近戰砍兩下的動作之距離設定");
                data.f_LBMelleAttackRange2 = EditorGUILayout.FloatField("f_LBMelleAttackRange2", data.f_LBMelleAttackRange2);
                EditorGUILayout.LabelField("大魔王之射擊距離");
                data.f_LBShootAttackRange = EditorGUILayout.FloatField("f_LBShootAttackRange", data.f_LBShootAttackRange);

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 追擊移動
            //追擊移動
            foldChase = EditorGUILayout.Foldout(foldChase, "★★★★各種追擊設定★★★★");
            if (foldChase)
            {
                EditorGUILayout.LabelField("追擊的走路速度，計算用");
                data.f_LBMoveSpeed = EditorGUILayout.FloatField("f_LBMoveSpeed", data.f_LBMoveSpeed);
                EditorGUILayout.LabelField("旋轉速度，計算用");
                data.f_LBRotationSpeed = EditorGUILayout.FloatField("f_LBRotationSpeed", data.f_LBRotationSpeed);
                EditorGUILayout.LabelField("是否在旋轉");
                data.b_LBRotating = EditorGUILayout.Toggle("b_LBRotating", data.b_LBRotating);
                EditorGUILayout.LabelField("是否在移動");
                data.b_LBMoving = EditorGUILayout.Toggle("b_LBMoving", data.b_LBMoving);
                EditorGUILayout.LabelField("魔王身上的Rigibody");
                data.rb = (Rigidbody)EditorGUILayout.ObjectField("rb", data.rb, typeof(Rigidbody), true);


                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 攻擊
            //攻擊
            foldAttack = EditorGUILayout.Foldout(foldAttack, "★★★★各種攻擊設定★★★★");
            if (foldAttack)
            {

                EditorGUILayout.LabelField("魔王跳起來移動的向量");
                data.v_LBJumpTeleport = EditorGUILayout.Vector3Field("v_LBJumpTeleport", data.v_LBJumpTeleport);


                //        

                //            




                //        

                //        EditorGUILayout.Space();

                //        

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 射擊
            foldShoot = EditorGUILayout.Foldout(foldShoot, "★★★★各種射擊設定★★★★");
            if (foldShoot)
            {
                EditorGUILayout.LabelField("射擊時的Emitter旋轉口是否可旋轉");
                data.b_LBEmitterRotate = EditorGUILayout.Toggle("b_LBEmitterRotate", data.b_LBEmitterRotate);

                EditorGUILayout.Space();
                GUI.backgroundColor = new Color(255.0f / 255.0f, 253f / 255f, 168f / 255f);
                EditorGUILayout.BeginVertical(GUI.skin.box);
                EditorGUILayout.LabelField("設定幾次的Idle之後一定要射大球");
                data.i_LBHowManyIdleTimesToShootBigBall = EditorGUILayout.IntField("i_LBHowManyIdleTimesToShootBigBall", data.i_LBHowManyIdleTimesToShootBigBall);
                GUI.backgroundColor = Color.white;
                EditorGUILayout.EndVertical();//畫個盒子結束    

                //大魔王泡泡發射口的群組
                EditorGUILayout.BeginVertical(GUI.skin.window);

                EditorGUILayout.LabelField("各個發射口的Holder，他們的爸爸");
                data.go_LBEmitterHolder = (GameObject)EditorGUILayout.ObjectField("go_LBEmitterHolder", data.go_LBEmitterHolder, typeof(GameObject), true);
                EditorGUILayout.LabelField("進戰砍一下的發射口Holder");
                data.go_LBAttackEmitterHolder = (GameObject)EditorGUILayout.ObjectField("go_LBAttackEmitterHolder", data.go_LBAttackEmitterHolder, typeof(GameObject), true);
                EditorGUILayout.Space();

                EditorGUILayout.LabelField("跳起來的腳下發射口");
                serializedObject.Update();
                SerializedProperty data_go_LBJumpShoot = serializedObject.FindProperty("go_LBJumpShoot");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_go_LBJumpShoot, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("發射圓圈");
                serializedObject.Update();
                SerializedProperty data_go_LBShootCircle = serializedObject.FindProperty("go_LBShootCircle");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_go_LBShootCircle, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("發射一顆大球");
                serializedObject.Update();
                SerializedProperty data_go_LBShootBigBall = serializedObject.FindProperty("go_LBShootBigBall");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_go_LBShootBigBall, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }

                EditorGUILayout.Space();

                EditorGUILayout.LabelField("發射三顆大球");
                serializedObject.Update();
                SerializedProperty data_go_LBShootBigBall2 = serializedObject.FindProperty("go_LBShootBigBall2");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_go_LBShootBigBall2, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }


                EditorGUILayout.Space();

                EditorGUILayout.LabelField("發射三顆會慢慢消失的持久型大球");
                serializedObject.Update();
    
                SerializedProperty data_go_LBShootBigBallTernado = serializedObject.FindProperty("go_LBShootBigBallTernado");
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(data_go_LBShootBigBallTernado, true);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedObject.ApplyModifiedProperties();
                }


                EditorGUILayout.Space();

                EditorGUILayout.EndVertical();//畫個盒子結束    
                EditorGUILayout.Space();
 

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

            #region 計時器
            //計時器
            foldTimer = EditorGUILayout.Foldout(foldTimer, "★★★★各種時間設定及監看★★★★");
            if (foldTimer)
            {
                EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                EditorGUILayout.LabelField("計時器");
                data.f_LBTimer = EditorGUILayout.FloatField("f_LBTimer", data.f_LBTimer);
                
                EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                //GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                //EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                //EditorGUILayout.LabelField("下次攻擊的計時器");
                //data.float_NextMelleAttack = EditorGUILayout.FloatField("float_NextMelleAttack", data.float_NextMelleAttack);
                //EditorGUILayout.LabelField("下次攻擊的時間設定");
                //data.float_MelleAttackRate = EditorGUILayout.FloatField("float_MelleAttackRate", data.float_MelleAttackRate);
                //GUI.backgroundColor = Color.white;
                //EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                //EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始
                //EditorGUILayout.LabelField("後退狀態的計時器");
                //data.backWaitTimerMelle = EditorGUILayout.FloatField("backWaitTimerMelle", data.backWaitTimerMelle);
                //EditorGUILayout.LabelField("後退狀態要當多久的時間設定");
                //data.backWaitTimeMelleSetting = EditorGUILayout.FloatField("backWaitTimeMelleSetting", data.backWaitTimeMelleSetting);
                //EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                //GUI.backgroundColor = new Color(176f / 255f, 196f / 255f, 222f / 255f);
                //EditorGUILayout.BeginVertical(GUI.skin.box);//畫個不同底色的盒子開始              
                //EditorGUILayout.LabelField("可以再次回到後退狀態的計時器");
                //data.LongLegCanBackAgainTimer = EditorGUILayout.FloatField("LongLegCanBackAgainTimer", data.LongLegCanBackAgainTimer);
                //EditorGUILayout.LabelField("可以再次後退的時間設定");
                //data.LongLegCanBackAgainTimeSetting = EditorGUILayout.FloatField("LongLegCanBackAgainTimeSetting", data.LongLegCanBackAgainTimeSetting);
                //GUI.backgroundColor = Color.white;
                //EditorGUILayout.EndVertical();//畫個不同底色的盒子結束

                EditorUtility.SetDirty(data);
            }
            EditorGUILayout.Space();
            #endregion

         
            EditorUtility.SetDirty(data);
        }
        #endregion


        EditorUtility.SetDirty(data);
    }
    #endregion
}

public class Fontsize : MonoBehaviour
{
    void OnGUI()
    {
        //ラベルが GUIStyle スタイルになるように設定
        GUIStyle style = GUI.skin.GetStyle("label");

        // styleのfont size　が経時的に大きくなったり小さくなったりするように設定 
        style.fontSize = (int)(20.0f + 10.0f * Mathf.Sin(Time.time));

        //ラベルを作って現在の設定で表示
        GUI.Label(new Rect(10, 10, 200, 80), "Hello World!");
    }

}
/* 可以同時編輯複數怪獸
//モンスタークラスを拡張
[CustomEditor(typeof(Monster))]

//複数選択有効
[CanEditMultipleObjects]

public class MonsterEditor : Editor
{
    //インスペクターを拡張
    public override void OnInspectorGUI()
    {

        Monster mainMonster = target as Monster;

        //注記
        EditorGUILayout.HelpBox("ステータスは0以下に設定できません。", MessageType.Info, true);

        //各ステータス
        mainMonster.HP = Mathf.Max(1, EditorGUILayout.FloatField("体力", mainMonster.HP));
        mainMonster.MP = Mathf.Max(1, EditorGUILayout.FloatField("マジックポイント", mainMonster.MP));
        mainMonster.Power = Mathf.Max(1, EditorGUILayout.FloatField("パワー！", mainMonster.Power));

        EditorUtility.SetDirty(target);

        //選択されている全てものについて処理
        foreach (Object monsterObject in targets)
        {

            Monster submonster = monsterObject as Monster;

            submonster.HP = mainMonster.HP;
            submonster.MP = mainMonster.MP;
            submonster.Power = mainMonster.Power;

            EditorUtility.SetDirty(monsterObject);
        }

    }

}
*/