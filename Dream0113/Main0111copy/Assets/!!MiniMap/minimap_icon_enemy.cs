﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;

public class minimap_icon_enemy : MonoBehaviour
{
    AI_data data;
    public GameObject thisEmeny;
    void Start()
    {
        data = GetComponentInParent<AI_data>();
        thisEmeny = data.enemy;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Slerp(transform.position, new Vector3(thisEmeny.transform.position.x, 120, thisEmeny.transform.position.z), 0.5f);
        transform.rotation = Quaternion.Euler(90, 0, 0);
    }
}
