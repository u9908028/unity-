﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject Target;
    Vector3 distance;
    public float smooth = 1f;
    //Camera mainCam;
    void Start()
    {
        distance = transform.position - Target.transform.position;
        //mainCam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(this.transform.position, Target.transform.position + distance, smooth);


    }
}
