﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;

public class NiceKyleBattleMission : MonoBehaviour
{
    [Header("事件觸發的點")]
    public TimelineManager tm_NiceKyleBattleMission;
    [Header("主角撞到了多少寶物")]
    public OnTrigger onTrigger;
    public int i_WatchGetTreasure=0;
    public int i_needToGetHowManyTreasure = 0;
    [Header("爆炸效果的物件池")]
    public ExplosionEffectPool explosionEffectPool;
    [Header("爆炸的計時器")]
    public float f_explosionTimer=0f;
    public float f_totalExplosionTimer=0f;
    public float f_howManyTimeToMakeAnExplosion=0.02f;
    public float f_totalExplosionTimeSetting=10f;
    public bool b_explosionHasOvered=false;
    [Header("要消失的障礙物們(只有Renderer，沒有Collider，Collider裝在爸爸身上)")]
    public GameObject[] go_Obstacles;
    public Collider _collider;
    [Header("爆炸效果的範圍")]
    public float f_explosionRangeX = 15;
    public float f_explosionRangeZ = 0;
    [Header("爆炸是否觸發")]
    public bool bStartExplosion;
    [Header("飛船登場後開啟collider讓玩家不能回頭")]
    public TimelineManager tmAirShipAppearedTri;
    [Header("音效")]
    public BombExplosionSoundEffectPool bombExplosionSoundEffectPool;
    public GameObject go_camera;
    // Start is called before the first frame update
    void Start()
    {
        onTrigger = GameObject.FindGameObjectWithTag("Controller").GetComponent<OnTrigger>();
        explosionEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ExplosionEffectPool>();
        tm_NiceKyleBattleMission = GameObject.Find("AnimEvent_03_上層平台Tri").GetComponent<TimelineManager>();
        _collider = GetComponent<Collider>();
        tmAirShipAppearedTri = GameObject.Find("AnimEvent_04_飛船登場_Tri").GetComponent<TimelineManager>();
        bombExplosionSoundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BombExplosionSoundEffectPool>();
        go_camera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        

        if (!tm_NiceKyleBattleMission.b_hasBeenTiggered) { return; }
        i_WatchGetTreasure = onTrigger.i_getTreasure;

        //按某個快捷鍵生出寶物
        if (Input.GetKeyDown(KeyCode.F1))
        {
            onTrigger.i_getTreasure = i_needToGetHowManyTreasure;
        }


        if (onTrigger.i_getTreasure >= i_needToGetHowManyTreasure)
        {
            bStartExplosion = true;
            MakeExplosion();
            CloseCollider();
            OpenCollider();
        }
        


    }
    
    void MakeExplosion()
    {
        if (b_explosionHasOvered) { return; }//爆炸時間總長結束之後就不要再進來這函式了
        f_explosionTimer += Time.deltaTime;
        f_totalExplosionTimer += Time.deltaTime;
        if(f_explosionTimer > f_howManyTimeToMakeAnExplosion)
        {
            Vector3 explosionPosition = new Vector3(transform.position.x + Random.Range(-f_explosionRangeX, f_explosionRangeX),
            transform.position.y, transform.position.z + Random.Range(f_explosionRangeZ, f_explosionRangeZ));
            Vector3 explosionPosition2= new Vector3(transform.position.x + Random.Range(-f_explosionRangeX, f_explosionRangeX),
            transform.position.y, transform.position.z + Random.Range(-f_explosionRangeZ, f_explosionRangeZ));
            explosionEffectPool.ReUse(explosionPosition, Quaternion.identity);
            explosionEffectPool.ReUse(explosionPosition2, Quaternion.identity);

            bombExplosionSoundEffectPool.ReUse(go_camera.transform.position, Quaternion.identity);
            //製造爆炸的計時器歸0
            f_explosionTimer = 0;
        }
        if (f_totalExplosionTimer > f_totalExplosionTimeSetting / 2f)
        {
            StartCoroutine(DestroyChildObject());
        }
        if (f_totalExplosionTimer>f_totalExplosionTimeSetting)
        {
            b_explosionHasOvered = true;
        }
    }

    public IEnumerator DestroyChildObject()
    {
        yield return new WaitForSeconds(2.5f);
        foreach (GameObject gameObject in go_Obstacles)
        {
            Destroy(gameObject);
        }

    }

    void CloseCollider()
    {
        if (!b_explosionHasOvered) { return; }//爆炸時間總長結束之後才可以關閉collider
        _collider.enabled = false;        
    }

    void OpenCollider()//飛船登場後重新開啟collider
    {
        if (tmAirShipAppearedTri.b_hasBeenTiggered && tmAirShipAppearedTri.timelineAnimation.state==UnityEngine.Playables.PlayState.Paused)
        {
            _collider.enabled = true;
        }
    }
}
