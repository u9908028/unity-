﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    public class AttackEvent : MonoBehaviour
    {
        // ***************用來觸發武器動的COLLIDER開關

        public GameObject weapeon;
        
        public Collider weapCol; //武器碰撞
        public Collider WarpCol;
        public Collider kickCol; 
        public GameObject[] VFX;
        public Transform endpos;
        public FightSystem fightSystem;
        public bool isWarp;
        public BladeFury bladeFury;
        public BladeFuryPool bladeFuryPool;
        public PowerChopPool powerChopPool;
        public Transform BladeFury_point;
        public Transform PowerChop_point;
        public bool isBladeFury;

        #region 音效相關
        [Header("音效物件池們")]
        public PlayerAttackSoundEffectPool playerAttackSoundEffectPool;
        public PlayerAttackSoundEffectPool_1 playerAttackSoundEffectPool_1;
        public PlayerAttackSoundEffectPool_2 playerAttackSoundEffectPool_2;
        public PlayerAttackSoundEffectPool_3 playerAttackSoundEffectPool_3;
        public GameObject go_mainCamera;
        [SerializeField] private int i_RandomPlayerAttackSound; 
        public void FindSoundPools()
        {
            go_mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
            playerAttackSoundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<PlayerAttackSoundEffectPool>();
            playerAttackSoundEffectPool_1 = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<PlayerAttackSoundEffectPool_1>();
            playerAttackSoundEffectPool_2 = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<PlayerAttackSoundEffectPool_2>();
            playerAttackSoundEffectPool_3 = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<PlayerAttackSoundEffectPool_3>();
        }
        #endregion



        private void Awake()
        {
           
            weapeon = GameObject.Find("Weapon_point");  //先抓到主物件以外的武器物件
            fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();    //print(weapeon.name);
            
            weapCol = weapeon.GetComponent<Collider>();  //先存入武器的碰撞器
            WarpCol = GameObject.Find("WarpCol").GetComponent<Collider>();// 閃現攻擊的Col
            kickCol = GameObject.Find("KickBox_R").GetComponent<Collider>();
            bladeFuryPool = GameObject.Find("ObjectPool").GetComponent<BladeFuryPool>();
            powerChopPool = GameObject.Find("ObjectPool").GetComponent<PowerChopPool>();
            PowerChop_point = GameObject.Find("PowerChop_point").GetComponent<Transform>();
            BladeFury_point = GameObject.Find("BladeFury_point").GetComponent<Transform>();
        }
        void Start()
        {
            weapCol.enabled = !enabled;//在還沒執行攻擊動作前, 先關閉武器的碰撞器 **** 已經影響到OPEN_COL這個Event 運行   //武器碰撞先關閉
            kickCol.enabled = !enabled;

            FindSoundPools();
        }
        public void FixedUpdate()
        {

        }
        public void Update()
        {

            // AttackEvent_1();
        }

        public void Event()
        {
            Debug.Log("It is work");
        }
        public void Close_Col()
        {
            weapCol.enabled = !enabled;
            //print(weapCol.enabled);
        }
        public void Open_Col() //攻擊動畫的Event判定碰撞器的開啟
        {
            weapCol.enabled = enabled;
            //print(weapCol.enabled);
            //print("Open_Col");
        }

        public void WarpStrike()
        {
            isWarp = true;

            Vector3 endpos = transform.position + transform.forward * 15f; //往前 15f
            transform.position = Vector3.Lerp(transform.position, endpos, 30f * Time.deltaTime);  //往前瞬間移動
            isWarp = false;
            //print("PowerStrike");
        }
        public void OpenWarpCol()
        {
            WarpCol.enabled = enabled;
        }
        public void CloseWarpstrike()
        {
            WarpCol.enabled = !enabled;
        }

        public void BladeFury()
        {
            //print("BladeFury");
            bladeFuryPool.ReUse(BladeFury_point.transform.position, BladeFury_point.transform.rotation);//                      
        }

        public void PowerChop()
        {
            print("PowerChop");
            powerChopPool.ReUse(PowerChop_point.transform.position, PowerChop_point.transform.rotation);
        }

        public void OpenKickCol()
        {
            kickCol.enabled = enabled;
        }

        public void CloseKickCol()
        {
            kickCol.enabled = !enabled;
        }
        
        public void DownUp()
        {
            fightSystem.state.moveSpeed = 7;
            fightSystem.state.runSpeed = 15;
            fightSystem.state.rotateSpeed = 8f;
        }

        public void LockMove()
        {
            fightSystem.state.moveSpeed = 0;
            fightSystem.state.runSpeed = 0;
            fightSystem.state.rotateSpeed = 0;
        }

        

        #region 冠余刀光
        //刀光
        [Header("刀光")]
        public GameObject SwordSlash;//刀光

        void Effect_action_func(int i)
        {
            VFX[i].SetActive(true);

            //effect[0].GetComponentInChildren<ParticleSystem>().loop=true;
            // effect[0].GetComponentInChildren<ParticleSystem>().Play(true);
            //GameObject gameObject= Instantiate(effect[0],effectTransform[0].position,effectTransform[0].rotation);
            // Destroy (gameObject);
            //print("這裡開始");
            StartCoroutine("WAITTOend", i);
        }
        IEnumerator WAITTOend(int i)
        {


            yield return new WaitForSeconds(0.6f);
            VFX[i].SetActive(false);
        }
        //刀光
        void Sword_action_func()
        {
            SwordSlash.SetActive(true);

            print("這裡開始");
            // StartCoroutine("WAITTOend");
        }
        void Sword_action_func_end()
        {
            SwordSlash.SetActive(false);
            //StartCoroutine("WAITTOend");

        }
        IEnumerator WAITTOend()
        {


            yield return new WaitForSeconds(0.02f);
            SwordSlash.SetActive(false);
        }

        #endregion

        #region 發出揮刀音效
        public void MakeAttackSound(int SoundKind)
        {
            //i_RandomPlayerAttackSound = Random.Range(1, 4);
            switch (SoundKind)
            {
                case 1:
                    playerAttackSoundEffectPool.ReUse(go_mainCamera.transform.position, Quaternion.identity);
                    break;
                case 2:
                    playerAttackSoundEffectPool_1.ReUse(go_mainCamera.transform.position, Quaternion.identity);
                    break;
                case 3:
                    playerAttackSoundEffectPool_2.ReUse(go_mainCamera.transform.position, Quaternion.identity);
                    break;
                case 4:
                    playerAttackSoundEffectPool_3.ReUse(go_mainCamera.transform.position, Quaternion.identity);
                    break;
            }
        }


        #endregion

    }

}
