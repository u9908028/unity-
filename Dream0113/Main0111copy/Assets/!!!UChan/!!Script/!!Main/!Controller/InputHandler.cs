﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class InputHandler : MonoBehaviour
    {
        float vertical;
        float horizontal;
        float jump;
        bool runInput;

        bool b_input;
        bool a_input;
        
        bool x_input;
        bool y_input;

        bool rb_input;
        float rt_axis;
        bool rt_input;
        bool lb_input;
        float lt_axis;
        bool lt_input;
        float jump_axis;
        bool jump_input;

        bool leftAxis_down;
        bool rigtAxis_down;
       

        StateManager states;
        CameraManager camManager;
        
        
        float delta;

        #region 尋找 / 控管 所有的Timeline事件的Trigger
        [Header("各種TimelineTri的腳本")]
        public TimelineManager tm_NiceKyleBeBullied;
        public TimelineManager tm_NiceKyleSpeak;
        public TimelineManager tm_NiceKyleBattleMissionStart;
        public TimelineManager tm_MissionComplete;
        public TimelineManager tm_AirShipAppeared;
        public TimelineManager tm_BossAppeared;
        public AI_data data_boss;
        public FightSystem fightSystem;

        public void FindAllTimelineTrigger()
        {
            tm_NiceKyleBeBullied = GameObject.FindGameObjectWithTag("KyleBeBulliedTimeline").GetComponent<TimelineManager>();
            tm_NiceKyleSpeak = GameObject.FindGameObjectWithTag("KyleSpeakTimeline").GetComponent<TimelineManager>();
            tm_NiceKyleBattleMissionStart = GameObject.Find("AnimEvent_03_上層平台Tri").GetComponent<TimelineManager>();
            tm_MissionComplete = GameObject.Find("AnimEvent_07_爆炸過程_Tri").GetComponent<TimelineManager>();
            tm_AirShipAppeared = GameObject.Find("AnimEvent_04_飛船登場_Tri").GetComponent<TimelineManager>();
            tm_BossAppeared = GameObject.Find("AnimEvent_05_boss登場Tri").GetComponent<TimelineManager>();
            data_boss = GameObject.Find("Boss").GetComponent<AI_data>();
            fightSystem = GameObject.FindGameObjectWithTag("FightSystem").GetComponent<FightSystem>();
        }

        /// <summary>
        /// 監測 Timeline 是否播放與其速度控制
        /// </summary>
        public void TimelinePlayingSpeedControl()
        {
            if (tm_NiceKyleBeBullied.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing ||
                tm_NiceKyleSpeak.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing ||
                tm_NiceKyleBattleMissionStart.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing ||
                tm_MissionComplete.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing ||
                tm_AirShipAppeared.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing ||
                tm_BossAppeared.timelineAnimation.state == UnityEngine.Playables.PlayState.Playing ||
                data_boss.GetHealthRate() <= 0f ||
                fightSystem.currentHp <=0)
            {
                //states.TimelinePlayingSpeedSettings();
                states.rigid.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY 
                    | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            }
            else 
            {
                //states.EverySpeedInitSettings();
                states.rigid.constraints = 
                     RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            }
        }
        #endregion

        void Start()
        {
            states = GetComponent<StateManager>();
            states.Init();

            camManager = CameraManager.singleton;
            camManager.Init(this.transform);
            //FollowMachine.FollowMachine_2 FM = new FollowMachine.FollowMachine_2();

            FindAllTimelineTrigger();
        }

        
        void FixedUpdate()
        {
            delta = Time.fixedDeltaTime;
            states.FixedTick(delta);
            GetInput();
            UpdateStates();
           

            //Debug.Log("FixedUpdate time :" + delta);
            camManager.Tick(delta);

            //播放Timeline時鎖腳色位移
            TimelinePlayingSpeedControl();
        }
        void Update()
        {
            delta = Time.deltaTime;
           // states.FixedTick(delta);
            states.Tick(delta);
            UpdateStates();
        }
        void GetInput()
        {
            vertical = Input.GetAxis("Vertical");
            horizontal = Input.GetAxis("Horizontal");
            b_input = Input.GetButton("b_input"); //跑
            a_input = Input.GetButton("A"); // 跟隨機
            
            x_input = Input.GetButton("X");
            y_input = Input.GetButton("Y");
            rt_axis = Input.GetAxis("RT");
            rt_input = Input.GetButtonDown("RT");
            if (rt_axis != 0)
                rt_input = true;

            jump_axis = Input.GetAxis("Jump");
            jump_input = Input.GetButton("Jump");
            if (jump_axis != 0)
                jump_input = true;

            lt_input = Input.GetButtonDown("LT");
            lt_axis = Input.GetAxis("LT");
            if (lt_axis != 0)
                lt_input = true;
            rb_input = Input.GetButtonDown("RB");
            lb_input = Input.GetButton("LB");

            rigtAxis_down = Input.GetButton("L"); //鎖定


        }
        void UpdateStates()
        {
            states.horizontal = horizontal;
            states.vertical = vertical;
            //states.jump = jump;
            Vector3 v = vertical * camManager.transform.forward;
            Vector3 h = horizontal * camManager.transform.right;
            //Vector3 j = jump * camManager.transform.up;
            states.moveDir = (v + h).normalized;
            float m = Mathf.Abs(horizontal) + Mathf.Abs(vertical);

            //states.turnMoveAmount = 0.3f * horizontal;
            states.moveAmount = Mathf.Clamp01(m);

            if (vertical != 0)
            {
                float RR = (Mathf.Sin(-vertical)) * states.TemP_angle * Mathf.Deg2Rad;
                states.turnMoveAmount = RR; //Turn數值**input  動畫用///////////*********8
                //print("vertical!=0 等於" + vertical);
            }
            else
            {
                float RR = horizontal;
                states.turnMoveAmount = horizontal; //Turn數值**input  動畫用///////////*********8
                vertical = 0;
            }


            if (b_input)
            {
                states.run = (states.moveAmount > 0);
                   
            }
            else
            {
                states.run = false;
            }
            states.jum = jump_input;
            states.rb = rb_input;
            states.rt = rt_input;
            states.lb = lb_input;
            states.lt = lt_input;
            states.Y = y_input; //換武器 姿勢
            states.A = a_input;

            if (y_input)
            {
                states.isTwoHanded = !states.isTwoHanded;
                states.HandleTwoWeapon();
            }
            if (rigtAxis_down)
            {
                states.lockOn = !states.lockOn;
                if(states.lockOnTarget == null)
                    states.lockOn = false;
                camManager.lockonTarget = states.lockOnTarget.transform;
                camManager.lockon =  states.lockOn;

                //Debug.Log("lock on target");
                //states.lockOn = true;// 目標鎖定後的bool保持

               // Debug.Log(camManager.lockonTarget);
            }
            
        }

    }
}

