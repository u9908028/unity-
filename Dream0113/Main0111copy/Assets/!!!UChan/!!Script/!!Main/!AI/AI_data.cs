﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Bubble;
namespace SA
{
    public class AI_data : MonoBehaviour
    {
        #region 找各種東西
        [Header("=====搜尋各個需要的東西====")]
        [Tooltip("這隻怪獸自己本身")]
        public GameObject enemy;//這個怪獸
        /// <summary>
        /// 這個怪獸的collider
        /// </summary>
        [Tooltip("這隻怪獸自己的Collider")]
        public Collider thisEnemyCollider;
        [Tooltip("射泡泡的腳本")]
        public BubbleShoot_3 bubbleShooter;//這個射擊機
        [Tooltip("玩家的物件")]
        public GameObject controller;
        [Tooltip("EnemyTarget的腳本")]
        public EnemyTarget enemyTarget;
        [Tooltip("FightSystem的腳本")]
        public FightSystem fightSystem;
        [Tooltip("補血道具的物件池")]
        public HealthPool healthPool;
        [Tooltip("怪獸身上的對話視窗上的腳本")]
        public KaiwaWindow kaiwaWindow;
        [Tooltip("雷射打中特效物件池")]
        public LaserHitEffectPool laserHitEffectPool;
        #endregion

        #region 往前追
        [Header("=====追蹤移動迴避=====")]
        [Tooltip("判斷障礙物的射線長度")] public float rayDistance = 2.0f;
        // Start is called before the first frame update
       

        [Tooltip("追擊的走路速度")]
        public float Chase_speed = 0.2f;//追擊的速度，計算用
        public float kyleSpeed;//凱爾初始追擊速度，設定用
        public float LongLegSpeed =0.2f;//長腳追擊速度，設定用
        public GameObject target;
        //public float AttackRange = 1.0f;//攻擊範圍
        [HideInInspector] [Tooltip("追擊的走路範圍")] public float ChaseRange = 1.0f;//追擊範圍
        [HideInInspector] public float nextChaseSpeed;
        [Tooltip("視角範圍")]
        public float SightRange = 1.0f;//視角範圍
        [Tooltip("面對玩家的轉動速度")]
        public float Rotationspeed = 0.2f;//轉動速度
        public float kyleRotation;
        [HideInInspector] public Rigidbody rb;
        [Tooltip("往前的速度，向量")]
        public Vector3 movementVec;//往前的速度
        [Tooltip("往前的速度，與玩家之間的向量")]
        public Vector3 dirToPlayer;//npc跟角色的向量
        [Tooltip("是不是在追擊")]
        public bool isChase;
        [Tooltip("下次追擊的計時器")]
        public float float_nextChase;
        [Tooltip("下次追擊的時間設定")]
        public float float_chaseRate;

        #endregion

        #region 跌倒
        [Header("====跌倒計時器====")]
        public float fallingBackTime;
        public float fallingBackWaitTime;
        public float fallingStateLeaveTimer;
        public float fallingStateLeaveTimeSetting;
        public float fallingToStandUpTimer;
        public float fallingToStandUpTimeSetting;
        #endregion

        #region 後退
        [Header("====後退相關====")]
        [Tooltip("是不是在追擊範圍")]
        public bool inBackRange;
        [Tooltip("是不是在後退狀態")]
        public bool isBack;
        [Tooltip("往後退的走路速度")]
        public float backSpeed;
        [Tooltip("長腳機器人往後的速度，設定用")]
        public float backSpeed_LongLeg=0.1f;
        [Tooltip("蜘蛛機器人後退的速度，設定用")]
        public float backSpeed_Spider = 0.2f;
        [Tooltip("進入往後退狀態的範圍")]
        public float backRang;
        [Tooltip("往後退狀態的計時器")]
        public float backWaitTimer;
        [Tooltip("往後退狀態的時間設定，要等幾秒")]
        public float backWaitTimeSetting;
        [Tooltip("射擊狀態要射多久變走路的計時器")]
        public float shootingToWalkingTimer;
        [Tooltip("射擊狀態要射多久變走路的時間設定")]
        public float shootingToWalkingTimeSetting;
        [Tooltip("攻擊狀態要打多久去走路的計時器")]
        public float LongLegCanBackAgainTimer;
        [Tooltip("攻擊狀態要打多久去走路的時間設定")]
        public float LongLegCanBackAgainTimeSetting;
        [Tooltip("往後退狀態要退多久的計時器，近戰用")]
        public float backWaitTimerMelle;
        [Tooltip("往後退狀態要退多久的時間設定，近戰用")]
        public float backWaitTimeMelleSetting;
        [HideInInspector] public float backWaitTimerKyle;
        [HideInInspector] public float backWaitTimeKyleSetting;
        [Tooltip("蜘蛛機器人可以退後的計時器")]
        public float f_SpiderCanBackAgainTimer;
        public float f_SpiderCanBackAgainTimeSetting;
        #endregion

        #region 血量
        [Header("=====血量=====")]
        [Tooltip("怪獸的最大血量")]
        public float maxHp;
        [Tooltip("怪獸現在的血量")]
        public float currentHp;
        [Tooltip("怪獸是不是死了")]
        public bool isDead;
        [Tooltip("怪獸死掉過了多久")]
        public float deathTimer;
        [Header("=====主角的傷害=====")]
        [Tooltip("主角傷害")]
        public float C_Damage;
        [Tooltip("主角打到幾下")]
        public int onTriCount;
        #endregion

        #region 距離
        
        [HideInInspector] public float float_Distance; //敵我雙方距離        
        [Header("=====距離判定=====")]
        [Tooltip("敵人的警戒範圍的距離")]
        public float float_checkInSight; //檢查敵人是否進入警戒範圍
        [Header("搜尋好人凱爾的距離")]
        public float f_BadKyleSearchNiceKyleDistance = 5f;
        [Tooltip("敵人的緩速區的距離")]
        public float float_SlowDownDistance;
        [HideInInspector] public float float_StopDistance;
        #endregion

        #region 沒用到
        [HideInInspector] public float melleAttackD_Damage;
        [HideInInspector] public float melleAttack_Range;

        //[Header("=====遠程攻擊力=====")]
        [HideInInspector] public float shoot_Damage;

        //[Header("=====攻擊間隔=====")]
        [HideInInspector] public float attackRate;
        [HideInInspector] public float nextAttack;
        #endregion

        #region Move所需要的變數 Move所需要的變數，目前未使用
        [Header("=====各種移動設定=====")]
        public bool bool_EnemyIsMoving;//移動狀態與否   

        public Vector3 vec_EnemyCurrentVector;//怪獸現在的向量
        public float float_EnemyCurrentMoveForce;//怪獸移動的力道
        public float float_EnemyCurrentMoveSpeed;//怪獸現在的移動速度
        public float float_EnemyMaxSpeed;//怪獸的最大移動速度

        [Header("=====各種轉向設定=====")]
        public float float_EnemyMaxRotateSpeed;//怪獸最大的轉向速度
        public float float_EnemyTempRotateForce;//怪獸暫時的轉向力道


        #endregion

        #region 攻擊&射擊所需要的變數
        [Header("=====是否進入攻擊狀態=====")]
        [Tooltip("是不是進入攻擊狀態")]
        public bool bool_Attack;

        #region 遠攻
        [Header("=====各種射擊設定=====")]
        [Tooltip("是否有遠攻屬性")]
        public bool bool_EnemyHasRemoteAttackProperty;//是否具有遠攻屬性
        [HideInInspector] public bool bool_RotateShootProperty;//是否具有旋轉射擊的屬性

        [Space(8)]
        [Tooltip("蜘蛛機器人的炮台")]
        public GameObject go_spiderTurret;//整個砲台
        [Tooltip("蜘蛛機器人砲台上的發射口")]
        public GameObject go_spiderTurretEmitter;//砲台上的發射口
        [Tooltip("蜘蛛機器人砲台的旋轉速度")]
        public float f_spiderTurretRotationSpeed = 1.5f;
        [Tooltip("蜘蛛機器人瞄準角度的補助用向量")]
        public Vector3 v_spiderAimingAngle;
        [Space(8)]
        [Tooltip("雷射蜘蛛機器人的雷射物件")]
        public GameObject go_LaserParent;
        [Tooltip("雷射蜘蛛機器人的雷射啟動光束")]
        public GameObject go_ChargeLaser;
        [Tooltip("雷射蜘蛛機器人的雷射最大威力光束")]
        public GameObject go_BeamLaser;
        [Tooltip("雷射蜘蛛機器人的雷射發射口的煙和粒子物件")]
        public GameObject go_SmokeAndSparks;
        [Tooltip("雷射蜘蛛機器人的雷射結束光束")]
        public GameObject go_EndLaser;
        [HideInInspector]
        public GameObject go_LaserCollider;
        [Tooltip("雷射發射的計時器，Function裡面用\n控制三種雷射啟動→最大→結束用")]
        public float f_LaserTimer;
        [Tooltip("其他狀態要隔多久才可以回到雷射狀態")]
        public float f_LaserRechargeTimeSetting;
        [Tooltip("其他狀態要隔多久才可以回到雷射狀態的計時器")]
        public float f_LaserRechargeTimer;
        [HideInInspector] public bool b_shootVisibleBubble;
        [HideInInspector] public bool b_shootInvisibleBubble;
        [HideInInspector] public bool b_laserColliderOn;
        [Tooltip("發射雷射時，每隔多久進行一次傷害判斷，射出Collider的速度")]
        public float f_LaserColliderShootSpeed;
        [Tooltip("發射雷射時，射出Collider的計時器")]
        public float f_LaserColliderShootTimer;
        //
        [Space(8)]
        [Tooltip("泡泡射擊速度，數字越大越慢(時間是倒數的)")]
        public float float_BubbleShootSpeed;//泡泡射擊速度，數字越大越慢(時間是倒數的)
        [Tooltip("第一次射泡泡的延遲時間")]
        public float float_FirstShootDelayTime;//第一次射泡泡的延遲時間
        [Tooltip("一波泡泡射擊總時間")]
        public float float_WaveShootTotalTime;//一波泡泡射擊總時間
        [Tooltip("泡泡射擊完之後的Delay時間，Delay完之後若條件達成繼續下一波")]
        public float float_WaveShootOverDelayTime;//泡泡射擊完之後的Delay時間，Delay完之後若條件達成繼續下一波
        [Tooltip("泡泡的發射口群組")]
        public Transform[] transArray_Emiiter;//泡泡發射口
        [HideInInspector] public int int_BubbleDamage;//泡泡的傷害

        //
        [HideInInspector] public float float_ShootingDistance;//射擊距離
        [HideInInspector] [Tooltip("砲台瞄準人的旋轉速度")] public float float_RotationSpeed;//砲台瞄準速度
        #endregion

        #region 沒用到的護頓
        [Header("=====護盾=====")]
        [Tooltip("護盾的腳本")]
        public Shield defenseShield;
        [Tooltip("有沒有護盾屬性")]
        public bool HaveShield;
        [Tooltip("護盾的物件")]
        public GameObject Shield;
        [Tooltip("護盾已打開")]
        public bool shieldOpen;
        [Tooltip("護盾的半徑")]
        public float shieldRadius;
        [Tooltip("護盾的能量")]
        public float shieldPower;
        #endregion

        #region 近戰
        [Header("=====各種近戰設定=====")]   // YEN
        public bool bool_EnemyHasMelleAttackProperty; //是否為近戰
        public bool bool_InAttackRange; //是否進入攻擊範圍
        public float float_LookRotationSpeed; //進入某警戒範圍後 會著面朝主角
        public float float_AttackRange; //攻擊範圍
        public float float_FirstAttackCountDown;
        public float float_FirstAttackDelayTime; //遇敵後的第一次攻擊時間
        public float float_MelleAttackRate; //攻擊間隔
        public float float_NextMelleAttack; //下一次的攻擊時間
        [Tooltip("凱爾進入狂暴的血量比例")]
        [Range(0.0f, 0.5f)] public float f_KyleCrazyHealthRate;//凱爾狂暴的血量比例
        public float f_KyleCrazyTimer;
        public float f_KyleCrazyTimeSetting;
        public int i_KyleCrazyKaiwaPattern;//凱爾狂暴狀態要開啟的對話視窗

        public int int_MelleAttack_Damage;//近戰攻擊傷害
        public string[] MelleAttackAnim; //攻擊的動畫陣列

        public Animator anim;

        [Header("=====承受傷害=====")]
        public bool takeDamage;
        public bool machineDamage;
        public bool bladeFuryDamage;
        public bool isFallingBackDown;
        public bool isStandUp;
        [Header("=====ＴＩＭＥ=====")]
        public bool IsFirstTimeAttack;//是不是第一次攻擊
        public bool IsFirstTimeShoot;
        #endregion
        #endregion

        #region 各種小兵之暴力換狀態變數
        [Header("====各種小兵之暴力換狀態用變數")]
        public int i_KyleStateNumber;
        public int i_LongLegStateNumber;
        public int i_SpiderStateNumber;
        public int i_LaserSpiderStateNumber;

        #endregion

        #region 大魔王所需要的變數

        [Header("====大魔王設定====")]
        public float f_LBMoveSpeed;
        public float f_LBRotationSpeed;
        [HideInInspector] public float f_LBdirToPlayer;//監控用
        public float f_LBTimer;
        [HideInInspector] public float f_LBTimerTemporarySave;//暫時存時間用的，要進入到下個狀態要等幾秒(動畫時間等待用)
        public int i_LBStatePattern;//大魔王的狀態哪一種，進入狀態的條件(為了random)
        [HideInInspector] Vector3 v_LBVectorToPlayer;//向量和上面的追逐共用
        public bool b_LBRotating;//是否在旋轉
        public bool b_LBMoving;//是否在移動
        [HideInInspector] public bool b_LBJumping;
        [HideInInspector] public bool b_LBAttacking;
        [HideInInspector] public Vector3 v_LBVectorChase;//往前追的向量
        //public Rigidbody rb_LBrb;
        [HideInInspector] public float f_LBAttackPrepareRange;//大魔王的攻擊距離
        public float f_LBMelleAttackRange1;//跳攻擊和短攻擊的距離，攻擊狀態1
        public float f_LBMelleAttackRange2;//常攻擊距離，攻擊狀態2
        [HideInInspector] public float f_LBDeciedAttackRange;//暫時儲存距離用的變數
        public float f_LBShootAttackRange;//射擊距離
        [HideInInspector] public int i_LBMelleAttackPattern;//random出了哪個攻擊，監看用
        [HideInInspector] public int i_LBShootAttackPattern;//遠攻樣式，監看用
        //public string[] s_LBShootAttackPatternAnimation;
        public bool b_LBEmitterRotate;//射擊時的旋轉口是否旋轉
        [HideInInspector] public float f_LBLeaveStateDistance;//離開狀態時的距離
        public int i_LBHowManyIdleTimesToShootBigBall;//設定幾次的Idle之後一定要射大球

        public float f_LastBossStartActionRange;//大魔王進到大魔王專屬的狀態的距離
        public Vector3 v_LBJumpTeleport;//魔王跳起來瞬間移動的向量
        [HideInInspector] public GameObject go_LBUnderCircleParticle;//魔王腳底下的光圈，跳起來瞬間移動的落下點

        [Header("大魔王發射泡泡的群組")]
        ///
        ///<summary>各emitter的爸爸=emitterholder</summary>
        public GameObject go_LBEmitterHolder;
        public GameObject go_LBAttackEmitterHolder;

        public GameObject[] go_LBJumpShoot;

        public GameObject[] go_LBShootCircle;

        public GameObject[] go_LBShootKome;

        public GameObject[] go_LBShootStar;

        public GameObject[] go_LBShootTriangle;

        public GameObject[] go_LBShootBigBall;

        public GameObject[] go_LBShootBigBall2;

        public GameObject[] go_LBShootAttack1;

        public GameObject[] go_LBShootBigBallTernado;

        public GameObject go_LBJumpShock;

        //下面存成Transform

        ////發射泡泡的各個函式
        //public void LastBossJumpShoot()
        //{
        //    bubbleShooter.MakeBubbleWithPools(tr_LBJumpShoot);
        //}


        #endregion

        #region 好人凱爾所需要的變數
        [Header("====好人凱爾變數設定")]
        public float f_NKTimer;//計時器
        public float f_NKSpeakRange;//講話的範圍
        public float f_NKMoveSpeed;//移動速度
        public float f_NKRotateSpeed;//旋轉速度
        public int i_NKStateNumber;//狀態的編號
        public bool b_NKRotate;//是否允許旋轉
        public Quaternion q_NKStartAngle;//初始角度
        public bool b_NKMove;//可不可以移動
        public GameObject[] otherEnemies;
        public float f_NKAttackRange;//好人凱爾攻擊範圍
        [HideInInspector] public Vector3 v_NKdirToTarget;
        [HideInInspector] public Vector3 v_NKmoveVector;
        public GameObject[] go_NKTargetPosition;
        public Vector3 v_NKAngleToTarget;
        public int i_NKTargetPositionIndex;

        public bool b_talkingKyle;
        public bool b_fightingKyle;
        public float f_DistanceOfAllEnemiesToNiceKyle;
        public bool b_enemiesInNiceKyleAttackRange;
        public GameObject go_nearestEnemyToNiceKyle;
        public float f_DistanceOfNearstEnemyToNiceKyle;
        //

        #endregion

        #region 壞人凱爾打好人凱爾
        public GameObject[] niceKyles;
        public GameObject go_nearestNiceKyleToBadKyle;
        public bool b_badKyleFindNiceKyle;
        public float f_DistanceOfNiceKyleToBadKyle;
        public float f_DistanceOfNearestNiceKyleToBadKyle;
        public bool b_BadKyleHitByPlayer;
        
        public float f_BadKyleAttackNiceKyleSpeed=0f;
        #endregion

        #region 雷射蜘蛛機器人的射線
        public LineRenderer lr_laserSpiderLaser;
        public Ray r_laserSpiderLaser;
        public LaserColliderPool laserColliderPool;
        public LaserMaker.LaserHitGroundEffectPool laserHitGroundEffectPool;
        public float f_laserHitGroundMakeEffectTime=0.25f;
        #endregion

        #region 凱爾牆
        public int i_KyleWallStateNumber;
        #endregion

        #region 魔王資料
        public AI_data boss_data;
        public EnemyTarget boss_target;
        #endregion

        #region 魔王跳起來的時候的畫面震動所需要的變數
        public Camera mainCamera;
        public GameObject go_mainCamera;
        public Vector3 v_mainCameraStartPosition;
        public CameraManager cameraManager;
        public GameObject cameraHolder;
        #endregion

        #region 魔王跳起來發出的震波
        public LBJumpShockWavePool LBJumpShockWavePool;
        #endregion

        #region 音效
        [Header("各種音效")]
        public EnemyBeHitEffectSoundPool enemyBeHitEffectSoundPool;
        public LastBossJumpEffectSoundPool Lastbossjumpeffectsoundpool;

        #endregion

        #region 上層平台戰鬥的任務所需變數
        [Space]
        [Header("上層平台戰鬥的任務所需變數")]
        public TreasurePool treasurePool;//寶藏的物件池
        public TimelineManager tm_NiceKyleBattleTri;//凱爾戰鬥事件的Trigger
        public TimelineManager tm_HeavenRoadTri;//天堂路的Trigger
        #endregion

        #region 天堂路的砲台所需變數
        [Space]
        [Header("天堂路砲台所需變數")]
        public int i_heavenTurrentState=0;
        public int i_heavenTurrentShootPattern=0;
        public TimelineManager tm_BossAppearedTri;
        public GameObject go_turrentEmitter;
        public Transform[] tr_turrentEmitter;
        public float f_heavenTurrentRotationSpeed=1.5f;
        #endregion       

        #region 雷射音效
        [Header("蜘蛛機器人的雷射音效")]        
        public bool b_SoundPlayStart=false;
        public LaserSoundEffectPool laserSoundEffectPool=null;
        public GameObject go_LaserSound;
        #endregion

        #region 掉落寶物的機率
        [Header("掉落寶物的機率")]
        public int i_dropItemRate=0;
        #endregion

        #region 掉落Wakizashi
        public WakizashiPool wakizashiPool;
        #endregion

        //2020/01/10
        #region 小地圖怪物圖標
        public Minimap_EnemyIcon_Pool minimap_enemyIconPool;
        public GameObject minimapIcon;
        #endregion

        //2020/01/11
        #region 確保著地的射線
        public RaycastHit hit;
        public LayerMask ignoreLayer;
        #endregion

        public void OnDrawGizmos()
        {
            Vector3 thisEnemyPosition = this.transform.position;
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(thisEnemyPosition, float_AttackRange);//劃出進戰攻擊範圍

            //Gizmos.color = Color.blue;
            //Gizmos.DrawWireSphere(thisEnemyPosition, float_checkInSight);//劃出進戰攻擊範圍

            //Gizmos.color = Color.blue;
            //Gizmos.DrawWireSphere(thisEnemyPosition, float_SlowDownDistance);

            //Gizmos.color = Color.red;
            //Gizmos.DrawWireSphere(this.transform.position, AttackRange);//攻擊範圍
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * rayDistance);

            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(thisEnemyPosition, backRang);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);//警戒範圍

            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(transform.position, f_BadKyleSearchNiceKyleDistance);
            //Gizmos.DrawWireSphere(this.transform.position, ChaseRange);//追擊範圍
            //Vector3 g1Rector = Quaternion.AngleAxis(-45f, Vector3.up) * transform.forward;

            //Vector3 g2Rector = Quaternion.AngleAxis(-22.5f, Vector3.up) * transform.forward;
            //Vector3 g3Rector = Quaternion.AngleAxis(22.5f, Vector3.up) * transform.forward;
            //Vector3 g4Rector = Quaternion.AngleAxis(45f, Vector3.up) * transform.forward;
            ////旁邊兩條

            //Gizmos.color = Color.magenta;
            //Gizmos.DrawLine(this.transform.position, this.transform.position + g1Rector * SightRange);
            //Gizmos.DrawLine(this.transform.position, this.transform.position + g4Rector * SightRange);
            ////
            //Gizmos.DrawLine(this.transform.position + g1Rector * SightRange, this.transform.position + g2Rector * SightRange);
            //Gizmos.DrawLine(this.transform.position + g2Rector * SightRange, this.transform.position + g3Rector * SightRange);
            //Gizmos.DrawLine(this.transform.position + g3Rector * SightRange, this.transform.position + g4Rector * SightRange);
            LastBossGizmos();
        }
        #region 魔王的線
        private void LastBossGizmos()
        {


            //Gizmos.color = Color.cyan;
            //Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward * 5.0f);

            //Gizmos.color = Color.white;
            //Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);

            if (enemyTarget == null)
            {
                return;
            }

            if (enemyTarget.currentState == StateType.Idle)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossFirstIdle)
            {
                Gizmos.color = new Color(0, 0.8f, 0);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossFirstShoot)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossChase1)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossJump1)
            {
                Gizmos.color = new Color(1.0f, 0.5f, 0);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossAttackNew1)
            {
                Gizmos.color = new Color(1.0f, 0, 0);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossShootBigBall)
            {
                Gizmos.color = new Color(1.0f, 0.6f, 0.6f);
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
            else if (enemyTarget.currentState == StateType.LastBossDeath)
            {
                Gizmos.color = Color.gray;
                Gizmos.DrawWireSphere(this.transform.position, float_checkInSight);
            }
        }
        #endregion

        public void Awake()
        {

            //AI_data一開始找東西的時候，先找自己，把該要的參考資料都找到
            enemy = this.gameObject;
            thisEnemyCollider = this.GetComponent<Collider>();
            bubbleShooter = enemy.GetComponentInChildren<BubbleShoot_3>();
            fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
            anim = GetComponent<Animator>();
            controller = GameObject.Find("Controller");
            currentHp = maxHp;
            rb = GetComponent<Rigidbody>();
            target = GameObject.Find("Controller");
            defenseShield = enemy.GetComponentInChildren<Shield>();
            //rb_LBrb = GetComponent<Rigidbody>();
            FindLastBossEmitters();//自動找發射點的函式，大魔王用
            healthPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<HealthPool>();
            enemyTarget = GetComponent<EnemyTarget>();

            //好人凱爾一開始的角度
            q_NKStartAngle = enemy.transform.rotation;
            otherEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            niceKyles = GameObject.FindGameObjectsWithTag("NiceKyle");

            //雷射射中特效物件池
            laserHitEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserHitEffectPool>();
            laserColliderPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserColliderPool>();

            //每個人都取得魔王身上的資料
            boss_target = GameObject.Find("Boss").GetComponent<EnemyTarget>();

            //記憶MainCamera的資訊
            mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            go_mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
            cameraManager = GameObject.Find("CameraHolder").GetComponentInChildren<CameraManager>();
            cameraHolder = GameObject.Find("CameraHolder");

            //魔王跳起來的震波
            LBJumpShockWavePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LBJumpShockWavePool>();

            //壞人凱爾打好人凱爾的攻擊速度
            f_BadKyleAttackNiceKyleSpeed = UnityEngine.Random.Range(3.5f, 6.0f);

            //音效
            enemyBeHitEffectSoundPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<EnemyBeHitEffectSoundPool>();
            laserSoundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserSoundEffectPool>();
            Lastbossjumpeffectsoundpool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LastBossJumpEffectSoundPool>();//boss跳起來音效

            //上層平台任務
            treasurePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<TreasurePool>();
            tm_NiceKyleBattleTri = GameObject.Find("AnimEvent_03_上層平台Tri").GetComponent<TimelineManager>();
            tm_HeavenRoadTri = GameObject.Find("AnimEvent_04_飛船登場_Tri").GetComponent<TimelineManager>();
            tm_BossAppearedTri = GameObject.Find("AnimEvent_05_boss登場Tri").GetComponent<TimelineManager>();

            //掉落Wakizashi
            wakizashiPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<WakizashiPool>();

            //小地圖圖標
            minimap_enemyIconPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<Minimap_EnemyIcon_Pool>();
            if(enemyTarget.b_IsKyle || enemyTarget.b_IsKyleWall || enemyTarget.b_IsLaserSpiderRobot || enemyTarget.b_IsLongLegRobot 
                || enemyTarget.b_IsSpiderRobot)
            {
                MakeMinmapIcon();
            }

            
        }


        private void Start()
        {
            FindKaiwaWindow();
            
            if (enemyTarget.b_IsKyle)
            {
                Chase_speed = kyleSpeed;

                Rotationspeed = kyleRotation;
            }
            else if (enemyTarget.b_IsLongLegRobot)
            {
                backSpeed = backSpeed_LongLeg;
            }
        }


        public void OnTriggerEnter(Collider other) //只能在第一個CLASS觸發
        {
            //凱爾牆的傷害判斷
            if (enemyTarget.b_IsKyleWall)
            {
                if (other.gameObject.tag == "Weapon_point")
                {
                    onTriCount++;
                    fightSystem.attack = true;
                    print("命中敵人");
                    takeDamage = true;
                    currentHp -= fightSystem.UpGradeDamage();
                    anim.SetTrigger("take damageTri");
                    enemyBeHitEffectSoundPool.ReUse(transform.position, Quaternion.identity);//被打音效
                    anim.SetTrigger("UndamageTri");
                }
                if (other.gameObject.tag == "Bullet")
                {
                    machineDamage = true;
                    currentHp -= fightSystem.machineDamage;
                }
                if (other.gameObject.tag == "warpCol")
                {
                    currentHp -= fightSystem.UpGradeDamage() * 2f;
                }
                if (other.gameObject.tag == "BladeFury")
                {
                    currentHp -= 200f;
                    enemyBeHitEffectSoundPool.ReUse(transform.position, Quaternion.identity);//被打音效
                }
            }

            if (other.gameObject.tag == "Weapon_point") /// 必須將主角武器設為"Weapon_point" !! 避免其他碰撞體傷害敵人
            {
                onTriCount++;
                fightSystem.attack = true;
                //print("命中敵人");
                takeDamage = true;
                currentHp -= fightSystem.UpGradeDamage();
                anim.SetTrigger("take damageTri");
                anim.SetTrigger("UndamageTri");
                enemyBeHitEffectSoundPool.ReUse(transform.position, Quaternion.identity);//被打音效
                //print("敵人當前血量 : " + currentHp);

                //壞人凱爾被主角打到了→強制從打好人凱爾狀態轉成Chase；Chase是不會進入到打凱爾狀態的，但是Idle會，所以進入打好凱爾狀態的條件是 bool=false和找到範圍內有好凱爾
                b_BadKyleHitByPlayer = true;
            }

            if (other.gameObject.tag == "warpCol")
            {
                currentHp -= fightSystem.UpGradeDamage() * 2f;

                //print("被閃現攻擊");
                enemyBeHitEffectSoundPool.ReUse(transform.position, Quaternion.identity);//被打音效
                //壞人凱爾被主角打到了→強制從打好人凱爾狀態轉成Chase；Chase是不會進入到打凱爾狀態的，但是Idle會，所以進入打好凱爾狀態的條件是 bool=false和找到範圍內有好凱爾
                b_BadKyleHitByPlayer = true;
            }
            //else
            //{
            //    //fightSystem.attack = false;
            //    anim.SetTrigger("UndamageTri");
            //    takeDamage = false;
            //}

            //bug : 射擊後  和武器一同造成傷害


            if (other.gameObject.tag == "Bullet") /// BUG: 會一直造成損傷 就算沒有射擊  
            {
                machineDamage = true;
                currentHp -= fightSystem.machineDamage;
                //anim.SetTrigger("take damageTri");

                //print("machineDamage");
                //takeDamege = false;

                //壞人凱爾被主角打到了→強制從打好人凱爾狀態轉成Chase；Chase是不會進入到打凱爾狀態的，但是Idle會，所以進入打好凱爾狀態的條件是 bool=false和找到範圍內有好凱爾
                b_BadKyleHitByPlayer = true;
            }
            else
            {
                //anim.SetTrigger("UndamageTri");
                machineDamage = false;
            }

            if (other.gameObject.tag == "BladeFury")
            {
                currentHp -= 200f;
                if (currentHp > 0)
                {
                    /*anim.SetTrigger("falling");*/ //播放被擊倒的動畫
                    bladeFuryDamage = true;
                    isFallingBackDown = true;
                }
                //anim.SetTrigger("take damageTri");    
                enemyBeHitEffectSoundPool.ReUse(transform.position, Quaternion.identity);//被打音效

                //壞人凱爾被主角打到了→強制從打好人凱爾狀態轉成Chase；Chase是不會進入到打凱爾狀態的，但是Idle會，所以進入打好凱爾狀態的條件是 bool=false和找到範圍內有好凱爾
                b_BadKyleHitByPlayer = true;
            }

            if (other.gameObject.tag == "PowerChop")
            {
                currentHp -= 1000f;
                bladeFuryDamage = true;
                isFallingBackDown = true;
                //anim.SetTrigger("take damageTri");
                enemyBeHitEffectSoundPool.ReUse(transform.position, Quaternion.identity);//被打音效

                //壞人凱爾被主角打到了→強制從打好人凱爾狀態轉成Chase；Chase是不會進入到打凱爾狀態的，但是Idle會，所以進入打好凱爾狀態的條件是 bool=false和找到範圍內有好凱爾
                b_BadKyleHitByPlayer = true;
            }

            //好人凱爾被打
            if (enemyTarget.b_IsNiceKyle)
            {
                if (other.gameObject.tag == "Enemy_Attack_BOX" /*&& enemyTarget.b_IsNiceKyle*/ )
                {
                    //Debug.Log("Hit");
                    anim.ResetTrigger("Attack1");
                    anim.ResetTrigger("Attack2");
                    anim.ResetTrigger("Attack3");
                    anim.SetTrigger("BeHit");
                }
            }


            if (other.gameObject.tag == "Laser")
            {
                if (enemyTarget.b_IsLastBoss || enemyTarget.b_IsKyleWall || enemyTarget.b_IsLaserSpiderRobot 
                    || enemyTarget.b_IsSpiderRobot || enemyTarget.b_IsLongLegRobot) { return; }
                currentHp -= 800f;
                anim.SetTrigger("take damageTri");
            }
            if (other.gameObject.tag == "Bomb")
            {
                if (enemyTarget.b_IsLastBoss || enemyTarget.b_IsKyleWall) { return; }
                currentHp -= 1200f;
                anim.SetTrigger("take damageTri");
            }
            if (other.gameObject.tag == "EnergyBall")
            {
                currentHp -= 1000f;
                anim.SetTrigger("take damageTri");
            }
            if(other.gameObject.tag == "KickBox_R")
            {
                currentHp -= 1000f;
                anim.SetTrigger("take damageTri");
            }
        }
        public void OnTriggerExit(Collider other) //  未 執行攻擊時 將weaponTrigger的attackTri 給關閉 //似乎邏輯不通?
        {
            if (other.gameObject.tag == "Weapon_point")
            {
                fightSystem.attack = false;
                takeDamage = false;
                machineDamage = false;
            }


        }

        public void OpenEnemyCol()
        {
            thisEnemyCollider.enabled = enabled;
        }

        public void CloseEnemyCol()
        {
            thisEnemyCollider.enabled = !enabled;
        }
        public float GetHealthRate()
        {
            return currentHp / maxHp;
        }
        //int IComparable<AI_data>.CompareTo(AI_data other)   // 使用IComparable 時必須加上去
        //{
        //    throw new NotImplementedException();
        //}

        public void FindLastBossEmitters()
        {
            #region 大魔王的發射點群組
            go_LBJumpShoot = GameObject.FindGameObjectsWithTag("JumpEmitter");
            go_LBShootBigBall = GameObject.FindGameObjectsWithTag("BigBallEmitter");
            go_LBShootCircle = GameObject.FindGameObjectsWithTag("CircleEmitter");
            go_LBShootKome = GameObject.FindGameObjectsWithTag("KomeEmitter");
            go_LBShootStar = GameObject.FindGameObjectsWithTag("StarEmitter");
            go_LBShootTriangle = GameObject.FindGameObjectsWithTag("TriangleEmitter");
            go_LBShootBigBallTernado = GameObject.FindGameObjectsWithTag("TernadoEmitter");
            go_LBShootAttack1 = GameObject.FindGameObjectsWithTag("ShootAttack1Emitter");
            go_LBEmitterHolder = GameObject.FindGameObjectWithTag("LB_EmitterHolder");

            go_LBAttackEmitterHolder = GameObject.FindGameObjectWithTag("LB_AttackEmitterHolder");
            go_LBShootBigBall2 = GameObject.FindGameObjectsWithTag("BigBallEmitter2");

            #endregion
        }
        
        public void FindKaiwaWindow()
        {
            if (enemyTarget.b_IsNiceKyle || enemyTarget.b_IsKyle)
            {
                kaiwaWindow = enemy.GetComponentInChildren<KaiwaWindow>();
            }

        }
        
        

        #region 壞人凱爾狂暴狀態掛在動畫上的Function
        public void KyleCrazyKaiwaWindowOpen1()
        {
            kaiwaWindow.b_controllerInRange = true;
            i_KyleCrazyKaiwaPattern = UnityEngine.Random.Range(1,4);
            switch (i_KyleCrazyKaiwaPattern)
            {
                case 1:
                    kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow1);
                    break;
                case 2:
                    kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow2);
                    break;
                case 3:
                    kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow3);
                    break;
            }                   
        }
        public void KyleCrazyKaiwaWindowOpen2()
        {
            kaiwaWindow.b_controllerInRange = true;
            kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow2);
        }
        public void KyleCrazyKaiwaWindowClose1()
        {
            kaiwaWindow.b_controllerInRange = false;
            switch (i_KyleCrazyKaiwaPattern)
            {
                case 1:
                    kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow1);
                    break;
                case 2:
                    kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow2);
                    break;
                case 3:
                    kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow3);
                    break;
            }
        }
        public void KyleCrazyKaiwaWindowClose2()
        {
            kaiwaWindow.b_controllerInRange = false;
            kaiwaWindow.ShowMessage(kaiwaWindow.go_kaiwaWindow2);
        }
        public void KyleCrazyMoveSpeedOpen()
        {
            Chase_speed = 0.08f;
        }
        public void KyleCrazyMoveSpeedClose()
        {
            Chase_speed = 0f;
        }
        #endregion

        #region 魔王掛在動畫上的Function
        public void LastBossMakeJumpShock()
        {
            GameObject jumpClone = Instantiate(go_LBJumpShock, this.transform.position, this.transform.rotation);
        }
        public void LastBossJumpTeleport()
        {

            enemy.transform.position = Vector3.Slerp(enemy.transform.position, enemy.transform.position + v_LBJumpTeleport, 1.0f);
        }
        //public void LastBossJumpTeleportCircleParticleOpen()
        //{
        //    lastBossJumpTeleportCirclePool.ReUse(enemy.transform.position + v_LBJumpTeleport, enemy.transform.rotation);
        //}
        public void LastBossJumpTeleportCircleParticleClose()
        {
            go_LBUnderCircleParticle.SetActive(false);
        }
        public void LastBossTeleportCircleMove()
        {

        }
        public void LastBossJumpSpeedOpen()
        {
            b_LBMoving = true;
            f_LBMoveSpeed = UnityEngine.Random.Range(0.3f, 0.5f);
        }
        public void LastBossJumpSpeedClose()
        {
            b_LBMoving = false;
            f_LBMoveSpeed = 0.0f;
        }

        public void CameraShake()
        {
            StartCoroutine(GamenShake(0.25f, 0.5f));
        }
        /// <summary>
        /// 震動攝影機位置
        /// </summary>
        /// <param name="darution">期間，共要多久 </param>
        /// <param name="magnitude">震度，震幅要多大</param>
        /// <returns></returns>
        private IEnumerator GamenShake(float darution, float magnitude)
        {
            //yield return new WaitForSeconds(0.1f);
            //cameraManager.Tick(Time.fixedDeltaTime);
            Vector3 startPos = cameraHolder.transform.position;//先把相機原來的位置記起來
            float _timer = 0f;
            while (_timer <darution)//當計時器小於期間的時候
            {
                float newX = cameraHolder.transform.position.x + UnityEngine.Random.Range(-1f, 1f) * magnitude;//給一個隨機的x和y值
                float newY = cameraHolder.transform.position.y + UnityEngine.Random.Range(-1f, 1f) * magnitude;

                cameraHolder.transform.position = new Vector3(newX, newY, cameraHolder.transform.position.z);

                //float newRX= go_mainCamera.transform.rotation.x + UnityEngine.Random.Range(-1f, 1f) * magnitude;

                //go_mainCamera.transform.rotation = Quaternion.Euler(newRX, go_mainCamera.transform.rotation.y, go_mainCamera.transform.rotation.z);

                _timer += Time.deltaTime;

                yield return null;
            }
            //go_mainCamera.transform.position = startPos;//做完震動之後要回到原位
            //cameraManager.Tick(Time.fixedDeltaTime);
        }

        public void LB_MakeJumpShockWave()
        {
            LBJumpShockWavePool.ReUse(transform.position + new Vector3(0, 0.1f, 0), Quaternion.Euler(270f, 0, 0));
        }

        public void LastBossJumpSound()
        {
            Lastbossjumpeffectsoundpool.ReUse(mainCamera.transform.position, Quaternion.identity);
        }
        #endregion

        #region 凱爾掉寶
        public void KyleDropTreasure()
        {
            if (i_dropItemRate <= 25f)
            {
                healthPool.ReUse(enemy.transform.position + new Vector3(0, 1, 0), enemy.transform.rotation);
            }
            else if (i_dropItemRate >= 26 && i_dropItemRate <= 75)//上層戰鬥事件的掉保率25%，要在啟動戰鬥事件後和天堂路Tri之前
            {
                if (tm_NiceKyleBattleTri.b_hasBeenTiggered && !tm_HeavenRoadTri.b_hasBeenTiggered)
                {
                    treasurePool.ReUse(enemy.transform.position + new Vector3(0, 1, 0), enemy.transform.rotation);
                }
            }
            //else if (i_dropItemRate >= 51 && i_dropItemRate <= 75)
            //{
            //    wakizashiPool.ReUse(enemy.transform.position + new Vector3(0, 1, 0), enemy.transform.rotation);
            //}
        }

        #endregion

        #region 受傷動作
        public void TakeDamageStart()
        {
            Chase_speed = 0;
            backSpeed = 0;
        }
        public void TakeDamageOver()
        {
            if (enemyTarget.b_IsKyle)
            {
                Chase_speed = kyleSpeed;
            }
            else if (enemyTarget.b_IsLongLegRobot)
            {
                Chase_speed = LongLegSpeed;
                backSpeed = backSpeed_LongLeg;
            }
        }

        #endregion

        #region 小地圖圖標
        private void MakeMinmapIcon()
        {
            minimapIcon = minimap_enemyIconPool.ReuseObject( new Vector3(enemy.transform.position.x, enemy.transform.position.y+1000, enemy.transform.position.z), Quaternion.identity);
        }

        #endregion
    }



}







