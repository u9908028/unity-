﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombHitGroundEffect : MonoBehaviour
{
    public BombHitGroundEffectPool bombHitGroundEffectPool;
    public float _timer;
    public float f_effectLife=30f;

    private void Awake()
    {
        bombHitGroundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BombHitGroundEffectPool>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToPool();
    }

    private void OnEnable()
    {
        _timer = Time.time;
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }

        if (Time.time > _timer + f_effectLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            bombHitGroundEffectPool.Recovery(this.gameObject);
        }
    }
}
