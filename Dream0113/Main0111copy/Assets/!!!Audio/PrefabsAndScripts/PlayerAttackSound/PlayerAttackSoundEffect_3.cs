﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackSoundEffect_3 : MonoBehaviour
{
    public PlayerAttackSoundEffectPool playerAttackSoundEffectPool;
    [Header("Sound Settings")]
    public AudioSource as_attackSound;
    [Header("Time Settings")]
    public float _timer = 0;
    public float lifeTime = 2.5f;
    // Start is called before the first frame update
    void Awake()
    {
        playerAttackSoundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<PlayerAttackSoundEffectPool>();
        as_attackSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToPool();
    }

    public void OnEnable()
    {
        _timer = Time.time;
        as_attackSound.Play();
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time > _timer+lifeTime)
        {
            playerAttackSoundEffectPool.Recovery(gameObject);
        }
    }
}
