﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSoundEffect : MonoBehaviour
{
    public LaserSoundEffectPool laserSoundEffectPool;
    [Header("Sound Setting")]
    public AudioSource as_laserSound;
    [Header("Time Setting")]
    public float _timer=0;
    public float lifeTime = 6f;
    [Header("Volume Settings")]
    public float f_volumeGrowRate=0.1f;
    public float f_volumeTimer;
    float _volume;

    // Start is called before the first frame update
    void Awake()
    {
        laserSoundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserSoundEffectPool>();
        as_laserSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //AdjustVolume();
    }

    private void OnEnable()
    {
        as_laserSound.volume = 0.01f;    
        as_laserSound.Play();
    }
    private void OnDisable()
    {
        as_laserSound.Stop();
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time > _timer+lifeTime)
        {
            as_laserSound.Stop();
            laserSoundEffectPool.Recovery(gameObject);
        }
    }
    public void AdjustVolume()
    {
        f_volumeTimer += Time.deltaTime;
        
        _volume += f_volumeGrowRate * f_volumeTimer;

        if (_volume>=0.2f)
        {
            _volume = 0.2f;
        }
        as_laserSound.volume = _volume;
    }
}
