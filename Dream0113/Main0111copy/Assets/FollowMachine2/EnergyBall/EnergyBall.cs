﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBall : MonoBehaviour
{
    [Header("物件池們")]
    public EnergyBallPool energyBallPool;
    public EnergyBallHitEnemyEffectPool energyBallHitEnemyEffectPool;
    [Header("速度")]
    public float f_speed=3f;

    [Header("能源球生命")]
    public float _timer;
    public float f_lifeTime=20f;
    public GameObject controller;//主角位置






    /// <summary>
    /// 生成時物件的初始
    /// </summary>
    private void Awake()
    {
        energyBallPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<EnergyBallPool>();
        controller = GameObject.FindGameObjectWithTag("Controller");
        energyBallHitEnemyEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<EnergyBallHitEnemyEffectPool>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        EnergyBallMove();
    }

    #region 移動
    void EnergyBallMove()
    {
        transform.position +=  transform.forward * f_speed * Time.deltaTime;
    }
    #endregion


    #region 生命

    private void OnEnable()
    {
        _timer = Time.time;//出生時記住時間   
    }

    private void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if(Time.time > _timer + f_lifeTime)
        {
            energyBallPool.Recovery(this.gameObject);
        }
    }

    private void EneryBallHitGround()
    {
        if (transform.position.y >= controller.transform.position.y - 0.15f && transform.position.y <= controller.transform.position.y + 0.15f)
        {
            StartCoroutine(RecycleEnergyBallHitEffect());
            StartCoroutine(RecycleEnergyBall());
        }
    }
    #endregion

    #region 攻擊

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            StartCoroutine(RecycleEnergyBallHitEffect());
            StartCoroutine(RecycleEnergyBall());
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Default"))
        {
            StartCoroutine(RecycleEnergyBallHitEffect());
            StartCoroutine(RecycleEnergyBall());
        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("Bubble"))
        {
            //StartCoroutine(RecycleEnergyBallHitEffect());
            StartCoroutine(RecycleEnergyBall());
        }
    }

    private IEnumerator RecycleEnergyBall()
    {
        yield return new WaitForSeconds(0.15f);
        energyBallPool.Recovery(this.gameObject);
    }
    private IEnumerator RecycleEnergyBallHitEffect()
    {
        GameObject go = energyBallHitEnemyEffectPool.ReuseObject(transform.position, transform.rotation);
        yield return new WaitForSeconds(1.0f);
        energyBallHitEnemyEffectPool.Recovery(go);
    }
    #endregion

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
    }
}
