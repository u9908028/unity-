﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;

public class ExtraFollowMachine : MonoBehaviour
{
    [Header("啟動的bool")]
    public bool b_missionComplete;  
    public NiceKyleBattleMission niceKyleBattleMission;

    [Header("玩家")]
    public GameObject controller;

    [Header("能源球發射時的音效")]
    public EnergyBallShootSoundEffectPool energyBallShootSoundEffectPool;
    public GameObject go_EnergyBallShootSound;



    // Start is called before the first frame update
    void Start()
    {
        niceKyleBattleMission = GameObject.FindGameObjectWithTag("HeavenRoadTri").GetComponent<NiceKyleBattleMission>();
        b_missionComplete = niceKyleBattleMission.bStartExplosion;
        controller = GameObject.FindGameObjectWithTag("Controller");
        //followMachine = GameObject.FindGameObjectWithTag("FollowMachine1").GetComponent<FollowMachine_4>();
        energyBallPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<EnergyBallPool>();
        boss_data = GameObject.Find("Boss").GetComponent<AI_data>();
        fightSystem = GameObject.FindGameObjectWithTag("FightSystem").GetComponent<FightSystem>();
        energyBallShootSoundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<EnergyBallShootSoundEffectPool>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        b_missionComplete = niceKyleBattleMission.bStartExplosion;
        if (!b_missionComplete) { return; }
        
        FloatingAndFollow();

        //取得敵人        
        BeHitEnemy();
        //抓腳色前面怪獸的血量
        GetEnemyData();
        //跟輔助機一號一樣，取得相同的敵人
        //GetBeHitEnemy();

        //自動射擊
        AutoShoot();
        //AutoShootAISwitch();
    }

    #region 上下漂浮 & 跟隨
    [Header("上下漂浮")]
    public float rangeOfArc = 0; //計算用，會一直加
    public float perRangeOfArc = 0.03f; //每秒漂浮變動量
    public float radius = 0.45f;//上下漂浮的範圍

    [Header("跟隨位置")]
    public float followPositionX;
    public float followPositionY;
    public float followPositionZ;
    private Vector3 tempVec = Vector3.zero;

    
    private void FloatingAndFollow()
    {
        if (b_Attack) { return; }       

        //計算一個會上下漂浮的Y座標
        rangeOfArc += perRangeOfArc;
        float dy = Mathf.Cos(rangeOfArc) * radius;
        
        //要去的座標位置是一個向量+主角位置
        Vector3 temp = new Vector3(followPositionX, followPositionY + dy, followPositionZ) + controller.transform.position;

        //用SmoothDamp去她的位置
        transform.position = Vector3.SmoothDamp(transform.position, temp, ref tempVec, 0.5f);

        //子物件自己旋轉
        SelfRotate();

        //跟隨主角轉向
        transform.rotation = controller.transform.rotation;
    }

    #endregion

    #region 自旋轉
    [Header("自旋轉")]
    public GameObject go_RotationObject;
    private float f_rotateZ =1.0f;
    public float f_rotateSpeed;    
    /// <summary>
    /// 子物件自己旋轉，沿Z軸方向
    /// </summary>
    private void SelfRotate()
    {
        f_rotateZ = 1.0f;
        go_RotationObject.transform.Rotate( 0, 0, f_rotateZ * f_rotateSpeed);
    }

    #endregion

    #region 自動射擊AI
    [Header("自動射擊")]
    public bool b_Attack;
    public FollowMachine_4 followMachine;
    public GameObject beHitEnemy;
    //public AI_data data_beHitEnemy;
    public RaycastHit beHitObject;
    public AI_data beHitEnemy_data;

    //public void GetBeHitEnemy()
    //{
    //    beHitEnemy = followMachine.BeHitEnemy();
    //    if (beHitEnemy != null)
    //    { 
    //        data_beHitEnemy = beHitEnemy.GetComponent<AI_data>();
    //    }
    //}

    [Header("射擊位置")]
    public float shootX;
    public float shootY;
    public float shootZ;

    [Header("發出物件的位置")]
    public GameObject emitter;

    [Header("射擊物件池")]
    public EnergyBallPool energyBallPool;


    [Header("射擊計時")]
    public float shootingTimer;
    public float shootSpeedSettingTime=5;

    [Header("射擊距離")]
    public float f_ShootingDistance = 15f;

    private void AutoShoot()
    {
        AutoShootAISwitch();
        if (b_Attack==false) { return; }              

        //攻擊的位置
        Vector3 shootingPosition = controller.transform.position + new Vector3(shootX, shootY, shootZ);
        transform.position = Vector3.Slerp(transform.position, shootingPosition, 0.1f);

        //射擊倒數
        shootingTimer -= Time.deltaTime;
        if (shootingTimer < 0)
        {
            go_EnergyBallShootSound = energyBallShootSoundEffectPool.ReUse(transform.position, Quaternion.identity);
            StartCoroutine(RecycleSound());
            energyBallPool.ReUse(transform.position, transform.rotation);
            shootingTimer = shootSpeedSettingTime;
        }

        //自動瞄準
        AutoAiming();

        //離開自動瞄準的判斷
        if (Vector3.Distance(controller.transform.position, beHitEnemy.transform.position) > f_ShootingDistance)
        {
            b_Attack = false;
            beHitEnemy = null;
        }

    }

    private IEnumerator RecycleSound()
    {
        yield return new WaitForSeconds(2.0f);
        energyBallShootSoundEffectPool.Recovery(go_EnergyBallShootSound);
        go_EnergyBallShootSound = null;
    }

    private void AutoAiming()
    {
        if (beHitEnemy_data.enemyTarget.b_IsLongLegRobot)
        {
            transform.rotation = Quaternion.LookRotation(beHitEnemy.transform.position + new Vector3(0, 2.0f , 0) - this.transform.position
                 , Vector3.up);
        }
        else
        {
            transform.rotation = Quaternion.LookRotation(beHitEnemy.transform.position + new Vector3(0, 0.8f, 0) - this.transform.position
                 , Vector3.up);
        }
    }

    #endregion

    #region 自動射擊AI的開關
    [Header("自動射擊AI的開關")]
    public AI_data boss_data;
    public FightSystem fightSystem;

    public void AutoShootAISwitch()
    {
        if (beHitEnemy==null || boss_data.GetHealthRate() <= 0 || fightSystem.currentHp <= 0 || beHitEnemy_data.currentHp <=0)
        {
            b_Attack = false;
        }
        else if (beHitEnemy != null )
        {
            b_Attack = true;
        }
    }

    //射線自動尋找敵人
    public GameObject BeHitEnemy()
    {
        //Debug.DrawRay(controller.transform.position + new Vector3(0, 0.5f, 0), controller.transform.forward*30f, Color.green);
        if (Physics.Raycast(controller.transform.position + new Vector3(0, 0.5f, 0), controller.transform.forward, out beHitObject, 30f))
        {
            //Debug.Log(beHitObject.transform.gameObject.name);
            if (beHitObject.collider.tag == "Enemy")
            {
                beHitEnemy = beHitObject.transform.gameObject;
                //v_beHitEnemyPosition = beHitObject.collider.bounds.center;
            }
        }
        return beHitEnemy;
    }

    /// <summary>
    /// 取得眼前的怪獸的資料
    /// </summary>
    public void GetEnemyData()
    {
        if (beHitEnemy == null) { return; }
        beHitEnemy_data = beHitEnemy.GetComponent<AI_data>();
    }
    #endregion


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position+transform.forward * 2 );
        Gizmos.color = Color.red;
        Gizmos.DrawLine(go_RotationObject.transform.position , go_RotationObject.transform.position
            +go_RotationObject.transform.forward);
        Gizmos.DrawLine(go_RotationObject.transform.position, go_RotationObject.transform.position
            + go_RotationObject.transform.right);
        Gizmos.DrawLine(go_RotationObject.transform.position, go_RotationObject.transform.position
            + go_RotationObject.transform.up);

    }
}
